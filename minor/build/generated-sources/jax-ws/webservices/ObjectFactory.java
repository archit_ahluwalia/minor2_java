
package webservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdateRating_QNAME = new QName("http://webservice/", "updateRating");
    private final static QName _GetViewResponse_QNAME = new QName("http://webservice/", "getViewResponse");
    private final static QName _GetRating_QNAME = new QName("http://webservice/", "getRating");
    private final static QName _GetView_QNAME = new QName("http://webservice/", "getView");
    private final static QName _GetRatingResponse_QNAME = new QName("http://webservice/", "getRatingResponse");
    private final static QName _UpdateRatingResponse_QNAME = new QName("http://webservice/", "updateRatingResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateRating }
     * 
     */
    public UpdateRating createUpdateRating() {
        return new UpdateRating();
    }

    /**
     * Create an instance of {@link GetViewResponse }
     * 
     */
    public GetViewResponse createGetViewResponse() {
        return new GetViewResponse();
    }

    /**
     * Create an instance of {@link UpdateRatingResponse }
     * 
     */
    public UpdateRatingResponse createUpdateRatingResponse() {
        return new UpdateRatingResponse();
    }

    /**
     * Create an instance of {@link GetView }
     * 
     */
    public GetView createGetView() {
        return new GetView();
    }

    /**
     * Create an instance of {@link GetRating }
     * 
     */
    public GetRating createGetRating() {
        return new GetRating();
    }

    /**
     * Create an instance of {@link GetRatingResponse }
     * 
     */
    public GetRatingResponse createGetRatingResponse() {
        return new GetRatingResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateRating }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "updateRating")
    public JAXBElement<UpdateRating> createUpdateRating(UpdateRating value) {
        return new JAXBElement<UpdateRating>(_UpdateRating_QNAME, UpdateRating.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetViewResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getViewResponse")
    public JAXBElement<GetViewResponse> createGetViewResponse(GetViewResponse value) {
        return new JAXBElement<GetViewResponse>(_GetViewResponse_QNAME, GetViewResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRating }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getRating")
    public JAXBElement<GetRating> createGetRating(GetRating value) {
        return new JAXBElement<GetRating>(_GetRating_QNAME, GetRating.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetView }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getView")
    public JAXBElement<GetView> createGetView(GetView value) {
        return new JAXBElement<GetView>(_GetView_QNAME, GetView.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRatingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getRatingResponse")
    public JAXBElement<GetRatingResponse> createGetRatingResponse(GetRatingResponse value) {
        return new JAXBElement<GetRatingResponse>(_GetRatingResponse_QNAME, GetRatingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateRatingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "updateRatingResponse")
    public JAXBElement<UpdateRatingResponse> createUpdateRatingResponse(UpdateRatingResponse value) {
        return new JAXBElement<UpdateRatingResponse>(_UpdateRatingResponse_QNAME, UpdateRatingResponse.class, null, value);
    }

}
