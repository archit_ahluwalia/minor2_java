package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.concurrent.TimeUnit;
import java.awt.*;
import java.io.*;
import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.entity.*;
import org.jfree.chart.labels.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.category.*;
import org.jfree.chart.urls.*;
import org.jfree.data.category.*;
import org.jfree.data.general.*;
import java.sql.*;

public final class chartt_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <title>Analysis</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"rating/jquery.rating.css\">\n");
      out.write("                <script src=\"rating/jquery.js\"></script>\n");
      out.write("                <script src=\"rating/jquery.rating.js\"></script>\n");
      out.write("       \n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\">\n");
      out.write("        <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"css/images/favicon.ico\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"all\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css1/css/style.css\" />\n");
      out.write("        <link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>\n");
      out.write("        <link href='http://fonts.googleapis.com/css?family=Jura:400,500,600,300' rel='stylesheet' type='text/css'>\n");
      out.write("        <script src=\"js/jquery-1.8.0.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"JS1/JS/jquery-1.4.2.min.js\"></script>\n");
      out.write("        <script src=\"JS1/JS/jquery.autocomplete.js\"></script>\n");
      out.write("        \n");
      out.write("\n");
      out.write("        <script src=\"js/jquery.touchwipe.1.1.1.js\"></script>\n");
      out.write("        <script src=\"js/jquery.carouFredSel-5.5.0-packed.js\"></script>\n");
      out.write("        <!--[if lt IE 9]><script src=\"js/modernizr.custom.js\"></script><![endif]-->\n");
      out.write("        <script src=\"js/functions.js\"></script>\n");
      out.write("    </head>\n");
      out.write("           \n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("         <div class=\"wrapper\">\n");
      out.write("      <!-- header -->\n");
      out.write("      <header class=\"header\">\n");
      out.write("        <div class=\"shell\">\n");
      out.write("          <div class=\"header-top\">\n");
      out.write("            <h1 id=\"logo\"><a href=\"#\">Gadget Review</a></h1>\n");
      out.write("            <nav id=\"navigation\"> <a href=\"#\" class=\"nav-btn\">Home<span></span></a>\n");
      out.write("              <ul>\n");
      out.write("                <li class=\"active home\">\n");
      out.write("                    <a href=\"home.jsp\">Home</a></li>\n");
      out.write("                <li><a href=\"www.twitter.com/gadgetaddp\">Twitter@gadgetaddp</a></li>\n");
      out.write("                <li><a href=\"contactus.jsp\">Contacts</a></li>\n");
      out.write("              </ul>\n");
      out.write("            </nav>\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("         <div class=\"slider\">\n");
      out.write("            <div id=\"bg\"></div>\n");
      out.write("            <div id=\"carousel\">\n");
      out.write("              <div>\n");
      out.write("                <h3>Analysis</h3>\n");
      out.write("                <p>Choose Your category</p>\n");
      out.write("                <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> \n");
      out.write("                <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> \n");
      out.write("                <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\">\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("         </div>\n");
      out.write("     </header>\n");
      out.write("      <!-- end of header -->\n");
      out.write("      <!-- shell -->\n");
      out.write("      <div class=\"shell\">\n");
      out.write("        <!-- main -->\n");
      out.write("        <div class=\"main\">\n");
      out.write("          <!-- cols -->\n");
      out.write("          <section class=\"cols\" id=\"specs\">\n");
      out.write("        <form action=\"chartform\" method=\"post\" >\n");
      out.write("           \n");
      out.write("        <select name=\"type\">\n");
      out.write("            <!--<option>View Analysis</option>-->\n");
      out.write("            <option>ave. Rating Analysis</option>\n");
      out.write("            <option>Comment Analysis</option>\n");
      out.write("        </select>\n");
      out.write("            <input type=\"submit\" />\n");
      out.write("        </form>\n");
      out.write("              \n");
      out.write("               <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </section>\n");
      out.write("               <!-- end of cols -->\n");
      out.write("      <section class=\"post\"> <!--<img src=\"css/images/a.jpg\" alt=\"\"> phone pic-->\n");
      out.write("            <div class=\"post-cnt\" >\n");
      out.write("  ");

  //TimeUnit.SECONDS.sleep(5); 

      out.write("\n");
      out.write("               \n");
      out.write("                \n");
      out.write(" </div>\n");
      out.write("            <div class=\"cl\" id=\"comments\">&nbsp;</div>\n");
      out.write("          </section>\n");
      out.write("                    \n");
      out.write("         \n");
      out.write("  <!--  <section class=\"partners\">\n");
      out.write("            <div id=\"partners-slider\">\n");
      out.write("              <div class=\"slider-holder2\"> <img src=\"css/images/a.jpg\" alt=\"\"> <img src=\"css/images/b.jpg\" alt=\"\"> <img src=\"css/images/c.jpg\" alt=\"\"> <img src=\"css/images/d.jpg\" alt=\"\"> <img src=\"css/images/a.jpg\" alt=\"\"> <img src=\"css/images/b.jpg\" alt=\"\"> <img src=\"css/images/c.jpg\" alt=\"\"> <img src=\"css/images/d.jpg\" alt=\"\"> </div>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"slider-arr\"> <a class=\"prev-arr arr-btn\" href=\"brands.jsp\"></a> <a class=\"next-arr arr-btn\" href=\"compare.jsp\"></a> </div>\n");
      out.write("          </section>\n");
      out.write("      -->    <div class=\"socials\">\n");
      out.write("            <p>We are currently <strong>available</strong> for work.</p>\n");
      out.write("            <ul>\n");
      out.write("              <li><a href=\"https://www.facebook.com/\" class=\"facebook-ico\">facebook-ico</a></li>\n");
      out.write("              <li><a href=\"https://twitter.com/\" class=\"twitter-ico\">twitter-ico</a></li>\n");
      out.write("              <li><a href=\"http://www.skype.com/en/\" class=\"skype-ico\">skype-ico</a></li>\n");
      out.write("              <li><a href=\"http://www.phonearena.com/\" class=\"rss-ico\">rss-ico</a></li>\n");
      out.write("            </ul>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- end of main -->\n");
      out.write("      </div>\n");
      out.write("      <!-- end of shell -->\n");
      out.write("      <!-- footer -->\n");
      out.write("      <div id=\"footer\">\n");
      out.write("        <!-- shell -->\n");
      out.write("        <div class=\"shell\">\n");
      out.write("          <!-- footer-cols -->\n");
      out.write("          <div class=\"footer-cols\">\n");
      out.write("\n");
      out.write("            <div class=\"col\">\n");
      out.write("              <h2>CONTACT us</h2>\n");
      out.write("              <p>Email: <a href=\"#\">addpminor@gmail.com</a></p>\n");
      out.write("              <p>Phone: 09899333927</p>\n");
      out.write("              <p>Address: Jaypee Institue of Information Techonology</p>\n");
      out.write("              <p>Noida</p>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("          <!-- end of footer-cols -->\n");
      out.write("          <div class=\"footer-bottom\">\n");
      out.write("            <div class=\"footer-nav\">\n");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                <li><a hrerf=\"home.jsp\">Home</a></li>\n");
      out.write("\n");
      out.write("                <li><a hrerf=\"www.twitter.com/gadgetaddp\">Twitter@gadgetaddp</a></li>\n");
      out.write("                <li><a hrerf=\"contactus.jsp\">Contacts</a></li>\n");
      out.write("              </ul>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <!--<p class=\"copy\">Copyright &copy; 2012<span>|</span>Design by: <a target=\"_blank\">ChocoTemplates</a></p>-->\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- end of shell -->\n");
      out.write("      </div>\n");
      out.write("      <!-- footer -->\n");
      out.write("    </div>\n");
      out.write("    <!-- end of wrapper -->\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
