package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import java.sql.*;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("<title>Gadget Analysis</title>\n");
      out.write("<meta charset=\"utf-8\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\">\n");
      out.write("<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"css/images/favicon.ico\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"all\">\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Jura:400,500,600,300' rel='stylesheet' type='text/css'>\n");
      out.write("<script src=\"js/jquery-1.8.0.min.js\"></script>\n");
      out.write("<script src=\"js/jquery.touchwipe.1.1.1.js\"></script>\n");
      out.write("<script src=\"js/jquery.carouFredSel-5.5.0-packed.js\"></script>\n");
      out.write("<!--[if lt IE 9]><script src=\"js/modernizr.custom.js\"></script><![endif]-->\n");
      out.write("<script src=\"js/functions.js\"></script>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("     <script>\n");
      out.write("        var x;\n");
      out.write("        var id;\n");
      out.write("  window.fbAsyncInit = function() {\n");
      out.write("  FB.init({\n");
      out.write("    appId      : '735114853176261',\n");
      out.write("    status     : true, // check login status\n");
      out.write("    cookie     : true, // enable cookies to allow the server to access the session\n");
      out.write("    xfbml      : true  // parse XFBML\n");
      out.write("  });\n");
      out.write("\n");
      out.write("    FB.Event.subscribe('auth.authResponseChange', function(response) {\n");
      out.write("    // Here we specify what we do with the response anytime this event occurs. \n");
      out.write("    if (response.status === 'connected') {\n");
      out.write("      // The response object is returned with a status field that lets the app know the current\n");
      out.write("      // login status of the person. In this case, we're handling the situation where they \n");
      out.write("      // have logged in to the app.\n");
      out.write("      testAPI();\n");
      out.write("       \n");
      out.write("      \n");
      out.write("    } else if (response.status === 'not_authorized') {\n");
      out.write("      // In this case, the person is logged into Facebook, but not into the app, so we call\n");
      out.write("      // FB.login() to prompt them to do so. \n");
      out.write("      // In real-life usage, you wouldn't want to immediately prompt someone to login \n");
      out.write("      // like this, for two reasons:\n");
      out.write("      // (1) JavaScript created popup windows are blocked by most browsers unless they \n");
      out.write("      // result from direct interaction from people using the app (such as a mouse click)\n");
      out.write("      // (2) it is a bad experience to be continually prompted to login upon page load.\n");
      out.write("      FB.login();\n");
      out.write("    } else {\n");
      out.write("      // In this case, the person is not logged into Facebook, so we call the login() \n");
      out.write("      // function to prompt them to do so. Note that at this stage there is no indication\n");
      out.write("      // of whether they are logged into the app. If they aren't then they'll see the Login\n");
      out.write("      // dialog right after they log in to Facebook. \n");
      out.write("      // The same caveats as above apply to the FB.login() call here.\n");
      out.write("      FB.login();\n");
      out.write("    }\n");
      out.write("  });\n");
      out.write("  };\n");
      out.write("\n");
      out.write("  // Load the SDK asynchronously\n");
      out.write("  (function(d){\n");
      out.write("   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];\n");
      out.write("   if (d.getElementById(id)) {return;}\n");
      out.write("   js = d.createElement('script'); js.id = id; js.async = true;\n");
      out.write("   js.src = \"//connect.facebook.net/en_US/all.js\";\n");
      out.write("   ref.parentNode.insertBefore(js, ref);\n");
      out.write("  }(document));\n");
      out.write("\n");
      out.write("  // Here we run a very simple test of the Graph API after login is successful. \n");
      out.write("  // This testAPI() function is only called in those cases. \n");
      out.write("  function testAPI() {\n");
      out.write("    console.log('Welcome!  Fetching your information.... ');\n");
      out.write("    FB.api('/me', function(response) {\n");
      out.write("        x=response.name;\n");
      out.write("        id=response.userID;\n");
      out.write("      console.log('Good to see you, ' + response.name + '.');\n");
      out.write("      //document.write(\"hey\"+response.name);\n");
      out.write("      // elem = document.getElementById(\"demo\");\n");
      out.write("        //            elem.innerHTML = x;\n");
      out.write("          //          elem2=document.getElementByid(\"forid\");\n");
      out.write("            //        elem2.innerHTML=id;\n");
      out.write("                   // var y=document.createElement(\"INPUT\");\n");
      out.write("                     var y=document.getElementById(\"fbname\");\n");
      out.write("                    y.setAttribute(\"value\",x);\n");
      out.write("    });\n");
      out.write("  }\n");
      out.write("</script>\n");
      out.write("<!-- wrapper -->\n");
      out.write("<div class=\"wrapper\">\n");
      out.write("  <!-- header -->\n");
      out.write("  <header class=\"header\">\n");
      out.write("    <div class=\"shell\">\n");
      out.write("      <div class=\"header-top\">\n");
      out.write("        <h1 id=\"logo\"><a href=\"#\">Gadget Review</a></h1>\n");
      out.write("        <nav id=\"navigation\"> <a href=\"#\" class=\"nav-btn\">Home<span></span></a>\n");
      out.write("          <ul>\n");
      out.write("            <li class=\"active home\"><a href=\"home.jsp\">Home</a></li>\n");
      out.write("            <li><a href=\"https://twitter.com/gadgetaddp\">Twitter@gadgetaddp</a></li>\n");
      out.write("            <li><a href=\"http://localhost:8080/contactus-war/\">Contact Us</a></li>\n");
      out.write("          </ul>\n");
      out.write("        </nav>\n");
      out.write("        <div class=\"cl\">&nbsp;</div>\n");
      out.write("      </div>\n");
      out.write("     <div class=\"slider\">\n");
      out.write("        <div id=\"bg\"></div>\n");
      out.write("        <div id=\"carousel\">\n");
      out.write("          <div>\n");
      out.write("            <h3>Specifications</h3>\n");
      out.write("            <p>To get the specifications of the image page</p>\n");
      out.write("            <a href=\"specs.jsp\" class=\"green-btn\">Specifications</a> <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\"> </div>\n");
      out.write("          <div>\n");
      out.write("            <h3>Search on specifications</h3>\n");
      out.write("            \n");
      out.write("            <a href=\"specificsearch.jsp\" class=\"green-btn\">Search</a> <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\"> </div>\n");
      out.write("         \n");
      out.write("            <div>\n");
      out.write("            <h3>Compare</h3>\n");
      out.write("            <p>Compare 2 phones based on the weightage for each category</p>\n");
      out.write("            <a href=\"compare.jsp\" class=\"green-btn\">Compare</a> <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\"> </div>\n");
      out.write("          <div>\n");
      out.write("            <h3>Analysis</h3>\n");
      out.write("            <p>\n");
      out.write("\t\t\tDisplays the graph of the most popluar phones and graph based on ratings\n");
      out.write("\t\t\t</p>\n");
      out.write("            <a href=\"chartt.jsp\" class=\"green-btn\">Analysis</a> <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\"> </div>\n");
      out.write("          <div>\n");
      out.write("            <h3>Brands</h3>\n");
      out.write("\t\t\t<p>Search for the post popular phones of a brand or select a phone as per brand</p>\n");
      out.write("            <a href=\"brand.jsp\" class=\"green-btn\">Brands</a> <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\"> </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"pagination\"></div>\n");
      out.write("        <a id=\"prev\" href=\"#\"></a><br> <a id=\"next\" href=\"#\"></a> </div>\n");
      out.write("    </div>\n");
      out.write("  </header>\n");
      out.write("  <!-- end of header -->\n");
      out.write("  <!-- shell -->\n");
      out.write("  <div class=\"shell\">\n");
      out.write("    <!-- main -->\n");
      out.write("    <div class=\"main\">\n");
      out.write("     \n");
      out.write("           <section class=\"cols\">\n");
      out.write("        <div class=\"col\">\n");
      out.write("          <div class=\"col-cnt\">\n");
      out.write("              <table style=\"width:300px\"><h2 style=\"text-align:center\"> Most Searched Phones</h2>\n");
      out.write("    \n");
      out.write("               ");
 
                   String id=null;
                    int view;
                    view=0;
                    Connection cn;
                    Statement st;
                   

                    Class.forName("com.mysql.jdbc.Driver");
                    cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/minor2","root","root");
                    st=cn.createStatement();
                    String sql="Select name from phone order by view desc";
                    ResultSet rs=st.executeQuery(sql);
                    int i=0;
     while(rs.next()&& i<10)
                                               {
         
      out.write("\n");
      out.write("         <tr><td style=\"width:300px;text-align:left\">\n");
      out.write("                <!-- <a href=\"specs.jsp?mbname=");
      out.print(rs.getString("name"));
      out.write("&username=\">-->\n");
      out.write("                <a href=\"specs.jsp?mbname=");
      out.print(rs.getString("name"));
      out.write("\" onclick=\"location.href=this.href+'&username='+x;return false;\">     \n");
      out.write("                     \n");
      out.write("         ");

                    out.println(rs.getString("name"));
                        i++;   
                
                    

      out.write("\n");
      out.write("                 </a> </td></tr>\n");
      out.write("         ");
 }
      out.write("\n");
      out.write("</table> \n");
      out.write("           </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col\"> \n");
      out.write("          <div class=\"col-cnt\">\n");
      out.write("              <table ><h2 style=\"text-align:center\">LATEST PHONES</h2>\n");
      out.write("    \n");
      out.write("               ");
 
                  
                    sql="Select name from phone order by date desc";
                    rs=st.executeQuery(sql);
                    i=0;
     while(rs.next()&& i<10)
                                               {
         
      out.write("\n");
      out.write("         <tr><td style=\"width:300px;text-align:left;list-style: disc\">\n");
      out.write("                <!-- <a href=\"specs.jsp?mbname=");
      out.print(rs.getString("name"));
      out.write("&username=\">-->\n");
      out.write("  <a href=\"specs.jsp?mbname=");
      out.print(rs.getString("name"));
      out.write("\" onclick=\"location.href=this.href+'&username='+x;return false;\">  \n");
      out.write("      \n");
      out.write("         ");

                    out.println(rs.getString("name"));
                        i++;   
                
                    

      out.write("\n");
      out.write("                 </a> </td></tr>\n");
      out.write("         ");
 }
      out.write("\n");
      out.write("</table>\n");
      out.write("             </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col\"> \n");
      out.write("          <div class=\"col-cnt\">\n");
      out.write("              <table style=\"width:325px;background-color:#FFFFCC\"><tr><td style=\"text-align:center\">\n");
      out.write("                          <h2>TECHNICAL NEWS</h2></td></tr>\n");
      out.write("               ");
 
                   Document doc = Jsoup.connect("http://www.technewsworld.com/perl/section/mobile-tech/").get();
            for(Element doc1 : doc.select("div.content-block table")){
                   
              
      out.write("\n");
      out.write("                      <tr>\n");
      out.write("                        <td> <ul>\n");
      out.write("                      <li> <a href=\"news.jsp?link=");
out.println(doc1.child(0).child(0).child(0).child(0).absUrl("href"));
      out.write("\">\n");
      out.write("                                    ");

                    out.println(doc1.child(0).child(0).child(0).child(0).html());

      out.write("\n");
      out.write("                             </a> </li>   </ul> \n");
      out.write("                          </td> \n");
      out.write("                  </tr>\n");
      out.write("                         \n");
      out.write("         ");

               }    
                    

      out.write("\n");
      out.write("       \n");
      out.write("</table>\n");
      out.write("           </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"cl\">&nbsp;</div>\n");
      out.write("      </section>\n");
      out.write("      <!-- end of cols -->\n");
      out.write("      <section class=\"post\"> \n");
      out.write("        <div class=\"post-cnt\">\n");
      out.write("          <h2>TWEETS FOR OUR WEBSITES</h2>\n");
      out.write("          <a align=\"right\" class=\"twitter-timeline\" href=\"https://twitter.com/gadgetaddp\" data-widget-id=\"448508029739102208\">Tweets by @gadgetaddp</a>\n");
      out.write("<script>\n");
      out.write("        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");\n");
      out.write("</script>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"cl\">&nbsp;</div>\n");
      out.write("      </section>\n");
      out.write("      <section class=\"content\">\n");
      out.write("        <h2>WHAT DOES OUR SITE OFFER</h2>\n");
      out.write("        \n");
      out.write("        <p> Seeing the  recent growing market for mobiles making a choice becomes difficult. And that is the reason for this site. Based on the user's review \n");
      out.write("\t\tprovided by user <span>tweets</span> and the <span>youtube vidoes</span> we are going to help the users by giving them a review of their fellow-users. This will help the users\n");
      out.write("\t\tin making the right decisions and decide on whats best for them.<br>\n");
      out.write("          <a href=\"#\" class=\"more\">meet the team</a></p>\n");
      out.write("      </section>\n");
      out.write("      <section class=\"partners\">\n");
      out.write("        <div id=\"partners-slider\">\n");
      out.write("          <div class=\"slider-holder2\"> <img src=\"css/images/a.jpg\" alt=\"\"> <img src=\"css/images/b.jpg\" alt=\"\"> <img src=\"css/images/c.jpg\" alt=\"\"> <img src=\"css/images/d.jpg\" alt=\"\"> <img src=\"css/images/a.jpg\" alt=\"\"> <img src=\"css/images/b.jpg\" alt=\"\"> <img src=\"css/images/c.jpg\" alt=\"\"> <img src=\"css/images/d.jpg\" alt=\"\"> </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"slider-arr\"> <a class=\"prev-arr arr-btn\" href=\"#\"></a> <a class=\"next-arr arr-btn\" href=\"#\"></a> </div>\n");
      out.write("      </section>\n");
      out.write("      <div class=\"socials\">\n");
      out.write("        <p>We are currently <strong>available</strong> for work.</p>\n");
      out.write("        <ul>\n");
      out.write("          <li><a href=\"https://www.facebook.com/\" class=\"facebook-ico\">facebook-ico</a></li>\n");
      out.write("          <li><a href=\"https://twitter.com/\" class=\"twitter-ico\">twitter-ico</a></li>\n");
      out.write("          <li><a href=\"http://www.skype.com/en/\" class=\"skype-ico\">skype-ico</a></li>\n");
      out.write("          <li><a href=\"http://www.phonearena.com/\" class=\"rss-ico\">rss-ico</a></li>\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("    <!-- end of main -->\n");
      out.write("  </div>\n");
      out.write("  <!-- end of shell -->\n");
      out.write("  <!-- footer -->\n");
      out.write("  <div id=\"footer\">\n");
      out.write("    <!-- shell -->\n");
      out.write("    <div class=\"shell\">\n");
      out.write("      <!-- footer-cols -->\n");
      out.write("      <div class=\"footer-cols\">\n");
      out.write("        \n");
      out.write("        \n");
      out.write("        <div class=\"col\">\n");
      out.write("          <h2>CONTACT us:</h2>\n");
      out.write("          <p>Email: <a href=\"#\">addpminor@gmail.com</a></p>\n");
      out.write("          <p>Phone: 09899333927</p>\n");
      out.write("          <p>Address: Jaypee Institue of Information Techonology</p>\n");
      out.write("          <p>Noida</p>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"cl\">&nbsp;</div>\n");
      out.write("      </div>\n");
      out.write("      <!-- end of footer-cols -->\n");
      out.write("      <div class=\"footer-bottom\">\n");
      out.write("        <div class=\"footer-nav\">\n");
      out.write("          <ul>\n");
      out.write("            <li><a hrerf=\"home.jsp\">Home</a></li>\n");
      out.write("            <li><a hrerf=\"https://twitter.com/gadgetaddp\">Twitter</a></li>\n");
      out.write("            <li><a hrerf=\"contactus.jsp\">Contacts</a></li>\n");
      out.write("          </ul>\n");
      out.write("        </div>\n");
      out.write("        <!--<p class=\"copy\">Copyright &copy; 2012<span>|</span>Design by: <a target=\"_blank\">ChocoTemplates</a></p>-->\n");
      out.write("        <div class=\"cl\">&nbsp;</div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("    <!-- end of shell -->\n");
      out.write("  </div>\n");
      out.write("  <!-- footer -->\n");
      out.write("</div>\n");
      out.write("<!-- end of wrapper -->\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
