package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class specificsearch_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("\n");
ResultSet resultset =null;
      out.write("\n");
      out.write("\n");
      out.write("<style type=\"text/css\">\n");
      out.write(".buttonprashi {\n");
      out.write("\t-moz-box-shadow:inset 0px 1px 32px 0px #cae3fc;\n");
      out.write("\t-webkit-box-shadow:inset 0px 1px 32px 0px #cae3fc;\n");
      out.write("\tbox-shadow:inset 0px 1px 32px 0px #cae3fc;\n");
      out.write("\tbackground:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #79bbff), color-stop(1, #4197ee) );\n");
      out.write("\tbackground:-moz-linear-gradient( center top, #79bbff 5%, #4197ee 100% );\n");
      out.write("\tfilter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#4197ee');\n");
      out.write("\tbackground-color:#79bbff;\n");
      out.write("\t-webkit-border-top-left-radius:20px;\n");
      out.write("\t-moz-border-radius-topleft:20px;\n");
      out.write("\tborder-top-left-radius:20px;\n");
      out.write("\t-webkit-border-top-right-radius:20px;\n");
      out.write("\t-moz-border-radius-topright:20px;\n");
      out.write("\tborder-top-right-radius:20px;\n");
      out.write("\t-webkit-border-bottom-right-radius:20px;\n");
      out.write("\t-moz-border-radius-bottomright:20px;\n");
      out.write("\tborder-bottom-right-radius:20px;\n");
      out.write("\t-webkit-border-bottom-left-radius:20px;\n");
      out.write("\t-moz-border-radius-bottomleft:20px;\n");
      out.write("\tborder-bottom-left-radius:20px;\n");
      out.write("\ttext-indent:0;\n");
      out.write("\tborder:1px solid #469df5;\n");
      out.write("\tdisplay:inline-block;\n");
      out.write("\tcolor:#ffffff;\n");
      out.write("\tfont-family:Arial;\n");
      out.write("\tfont-size:15px;\n");
      out.write("\tfont-weight:bold;\n");
      out.write("\tfont-style:normal;\n");
      out.write("\theight:40px;\n");
      out.write("\tline-height:40px;\n");
      out.write("\twidth:100px;\n");
      out.write("\ttext-decoration:none;\n");
      out.write("\ttext-align:center;\n");
      out.write("\ttext-shadow:1px 1px 0px #287ace;\n");
      out.write("}\n");
      out.write(".buttonprashi:hover {\n");
      out.write("\tbackground:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4197ee), color-stop(1, #79bbff) );\n");
      out.write("\tbackground:-moz-linear-gradient( center top, #4197ee 5%, #79bbff 100% );\n");
      out.write("\tfilter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#4197ee', endColorstr='#79bbff');\n");
      out.write("\tbackground-color:#4197ee;\n");
      out.write("}.buttonprashi:active {\n");
      out.write("\tposition:relative;\n");
      out.write("\ttop:1px;\n");
      out.write("}</style>\n");
      out.write("\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("<title>Specifications</title>\n");
      out.write("\n");
      out.write("<meta charset=\"utf-8\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\">\n");
      out.write("<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"css/images/favicon.ico\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"all\">\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"css1/css/style.css\" />\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Jura:400,500,600,300' rel='stylesheet' type='text/css'>\n");
      out.write("<script src=\"js/jquery-1.8.0.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"JS1/JS/jquery-1.4.2.min.js\"></script>\n");
      out.write("<script src=\"JS1/JS/jquery.autocomplete.js\"></script>\n");
      out.write("\n");
      out.write(" \n");
      out.write("\n");
      out.write("<script src=\"js/jquery.touchwipe.1.1.1.js\"></script>\n");
      out.write("<script src=\"js/jquery.carouFredSel-5.5.0-packed.js\"></script>\n");
      out.write("<!--[if lt IE 9]><script src=\"js/modernizr.custom.js\"></script><![endif]-->\n");
      out.write("<script src=\"js/functions.js\"></script>\n");
      out.write(" <script>\n");
      out.write("        var x;\n");
      out.write("        var id;\n");
      out.write("  window.fbAsyncInit = function() {\n");
      out.write("  FB.init({\n");
      out.write("    appId      : '735114853176261',\n");
      out.write("    status     : true, // check login status\n");
      out.write("    cookie     : true, // enable cookies to allow the server to access the session\n");
      out.write("    xfbml      : true  // parse XFBML\n");
      out.write("  });\n");
      out.write("\n");
      out.write("    FB.Event.subscribe('auth.authResponseChange', function(response) {\n");
      out.write("    // Here we specify what we do with the response anytime this event occurs. \n");
      out.write("    if (response.status === 'connected') {\n");
      out.write("      // The response object is returned with a status field that lets the app know the current\n");
      out.write("      // login status of the person. In this case, we're handling the situation where they \n");
      out.write("      // have logged in to the app.\n");
      out.write("      testAPI();\n");
      out.write("       \n");
      out.write("      \n");
      out.write("    } else if (response.status === 'not_authorized') {\n");
      out.write("      // In this case, the person is logged into Facebook, but not into the app, so we call\n");
      out.write("      // FB.login() to prompt them to do so. \n");
      out.write("      // In real-life usage, you wouldn't want to immediately prompt someone to login \n");
      out.write("      // like this, for two reasons:\n");
      out.write("      // (1) JavaScript created popup windows are blocked by most browsers unless they \n");
      out.write("      // result from direct interaction from people using the app (such as a mouse click)\n");
      out.write("      // (2) it is a bad experience to be continually prompted to login upon page load.\n");
      out.write("      FB.login();\n");
      out.write("    } else {\n");
      out.write("      // In this case, the person is not logged into Facebook, so we call the login() \n");
      out.write("      // function to prompt them to do so. Note that at this stage there is no indication\n");
      out.write("      // of whether they are logged into the app. If they aren't then they'll see the Login\n");
      out.write("      // dialog right after they log in to Facebook. \n");
      out.write("      // The same caveats as above apply to the FB.login() call here.\n");
      out.write("      FB.login();\n");
      out.write("    }\n");
      out.write("  });\n");
      out.write("  };\n");
      out.write("\n");
      out.write("  // Load the SDK asynchronously\n");
      out.write("  (function(d){\n");
      out.write("   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];\n");
      out.write("   if (d.getElementById(id)) {return;}\n");
      out.write("   js = d.createElement('script'); js.id = id; js.async = true;\n");
      out.write("   js.src = \"//connect.facebook.net/en_US/all.js\";\n");
      out.write("   ref.parentNode.insertBefore(js, ref);\n");
      out.write("  }(document));\n");
      out.write("\n");
      out.write("  // Here we run a very simple test of the Graph API after login is successful. \n");
      out.write("  // This testAPI() function is only called in those cases. \n");
      out.write("  function testAPI() {\n");
      out.write("    console.log('Welcome!  Fetching your information.... ');\n");
      out.write("    FB.api('/me', function(response) {\n");
      out.write("        x=response.name;\n");
      out.write("        id=response.userID;\n");
      out.write("      console.log('Good to see you, ' + response.name + '.');\n");
      out.write("      //document.write(\"hey\"+response.name);\n");
      out.write("      // elem = document.getElementById(\"demo\");\n");
      out.write("        //            elem.innerHTML = x;\n");
      out.write("          //          elem2=document.getElementByid(\"forid\");\n");
      out.write("            //        elem2.innerHTML=id;\n");
      out.write("                   // var y=document.createElement(\"INPUT\");\n");
      out.write("                     var y=document.getElementById(\"fbname\");\n");
      out.write("                    y.setAttribute(\"value\",x);\n");
      out.write("    });\n");
      out.write("  }\n");
      out.write("</script>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <div id=\"fb-root\"></div>\n");
      out.write("    \n");
      out.write("\n");
      out.write("<!-- wrapper -->\n");
      out.write("<div class=\"wrapper\">\n");
      out.write("      <!-- header -->\n");
      out.write("      <header class=\"header\">\n");
      out.write("        <div class=\"shell\">\n");
      out.write("          <div class=\"header-top\">\n");
      out.write("            <h1 id=\"logo\"><a href=\"#\">Gadget Review</a></h1>\n");
      out.write("            <nav id=\"navigation\"> <a href=\"#\" class=\"nav-btn\">Home<span></span></a>\n");
      out.write("              <ul>\n");
      out.write("                <li class=\"active home\">\n");
      out.write("                    <a href=\"home.jsp\">Home</a></li>\n");
      out.write("                <li><a href=\"www.twitter.com/gadgetaddp\">Twitter@gadgetaddp</a></li>\n");
      out.write("                <li><a href=\"contactus.jsp\">Contacts</a></li>\n");
      out.write("              </ul>\n");
      out.write("            </nav>\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("         <div class=\"slider\">\n");
      out.write("            <div id=\"bg\"></div>\n");
      out.write("            <div id=\"carousel\">\n");
      out.write("              <div>\n");
      out.write("                <h3>Search by specification</h3>\n");
      out.write("              \n");
      out.write("                <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> \n");
      out.write("                <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> \n");
      out.write("                <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\">\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("         </div>\n");
      out.write("     </header>\n");
      out.write("      <!-- end of \n");
      out.write("  <!-- end of header -->\n");
      out.write("  <!-- shell -->\n");
      out.write("  <div class=\"shell\">\n");
      out.write("    <!-- main -->\n");
      out.write("    <div class=\"main\">\n");
      out.write("      <!-- cols -->\n");
      out.write("      <section class=\"cols\">\n");
      out.write("                    \n");
      out.write("          <form action=\"specificsearch.jsp\" method=\"post\">\n");
      out.write("              <h2>Select the specification you want :</h2>\n");
      out.write("                  ");
 
                  Class.forName("com.mysql.jdbc.Driver");
                  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/minor2","root","root");
                  
                  Statement statement = connection.createStatement() ;
                      
      out.write("\n");
      out.write("                  <table>\n");
      out.write("                      <tr>\n");
      out.write("                          <td>\n");
      out.write("                  Camera:\n");
      out.write("                          </td>\n");
      out.write("                          <td>\n");
      out.write("                              <input type=\"text\" name=\"val1\">\n");
      out.write("                          </td>   \n");
      out.write("                  </tr>\n");
      out.write("                      <tr>\n");
      out.write("                          <td>\n");
      out.write("                  O.S.:\n");
      out.write("                      \n");
      out.write("                   \n");
      out.write("                          </td>\n");
      out.write("                             <td>\n");
      out.write("                              <input type=\"text\" name=\"val2\">\n");
      out.write("                          </td> \n");
      out.write("                  </tr>\n");
      out.write("                      <tr>\n");
      out.write("                          <td>\n");
      out.write("                 InBuilt Memory:\n");
      out.write("                   \n");
      out.write("                          </td>\n");
      out.write("                             <td>\n");
      out.write("                              <input type=\"text\" name=\"val3\">\n");
      out.write("                          </td> \n");
      out.write("                  </tr>\n");
      out.write("                  \n");
      out.write("                  </table>\n");
      out.write("          <input class=\"buttonprashi\" type=\"submit\" /></h2>\n");
      out.write("          </form>\n");
      out.write("                     ");

                     String table1=null,table2=null,table3=null;
                     String col1=null,col2=null,col3=null;
                  //  String cat1=request.getParameter("category1");
                     //String cat2=request.getParameter("category2");
                     //String cat3=request.getParameter("category3");
                    String val1=request.getParameter("val1");
                    String val2=request.getParameter("val2");
                    String val3=request.getParameter("val3");
                    
      out.write("\n");
      out.write("                   \n");
      out.write("                        ");
//out.println(val1);
      out.write("\n");
      out.write("                        ");
 //out.println(val2);
      out.write("\n");
      out.write("                        ");
 //out.println(val3);
      out.write("\n");
      out.write("                        ");
// out.println(cat1);
      out.write("\n");
      out.write("                        ");
//out.println(cat2);
      out.write("\n");
      out.write("                        ");
// out.println(cat3);
      out.write("\n");
      out.write("                   \n");
      out.write("          ");

                  //  if(cat1!=null && cat2!=null && cat3!=null){
//                                         if(cat1=="Camera"){
//                         col1="camera";
//                         table1="camera";
//                     }
//                     if(cat1=="Display size")
//                        {
//                      col1="size";
//                      table1="display";
//                      }
//                      if(cat1=="O.S")
//                        {
//                      col1="operating_system";
//                      table1="software";
//                      }
//                      if(cat1=="Inbuilt Memory")
//                        {
//                      col1="inbuilt";
//                      table1="memory";
//                      }
//                     if(cat2=="Camera"){
//                         col2="camera";
//                         table2="camera";
//                     }
//                      if(cat2=="Display size")
//                        {
//                      col2="size";
//                      table2="display";
//                      }
//                      if(cat2=="O.S")
//                        {
//                      col2="operating_system";
//                      table2="software";
//                      }
//                      if(cat2=="Inbuilt Memory")
//                        {
//                      col2="inbuilt";
//                      table2="memory";
//                      }
//                     if(cat3=="Camera"){
//                         col3="camera";
//                         table3="camera";
//                     }
//                      if(cat3=="Display size")
//                        {
//                      col3="size";
//                      table3="display";
//                      }
//                      if(cat3=="O.S")
//                        {
//                      col3="operating_system";
//                      table3="software";
//                      }
//                      if(cat3=="Inbuilt Memory")
//                        {
//                      col3="inbuilt";
//                      table3="memory";
//                      }
//                     
                     col1="camera";
              table1="camera";
             col2="operating_system";
             table2="software";
             col3="inbuilt";
             table3="memory";
                     
      out.write("\n");
      out.write("          \n");
      out.write("       <div class=\"cl\">&nbsp;</div>\n");
      out.write("      </section>\n");
      out.write("      <!-- end of cols -->\n");
      out.write("  <section class=\"post\"> \n");
      out.write("        <div class=\"post-cnt\" id=\"specs\">\n");
      out.write("            <h2>\n");
      out.write("                ");

              //  out.println(col1+col2+col3+table1+table2+table3);
                
      out.write("\n");
      out.write("            </h2>\n");
      out.write("             ");
 
             if(val1!=null|| val2!=null|| val3!=null){
            String id=null;
              Connection cn;
              Statement st;
              String[] a=new String[884];
              String[] b=new String[884];
              String[] c=new String[884];
              int[][] matrix=new int[3][884];
              String[] phone=new String[884];
              int i=0;
              ResultSet rs;
              
                             Class.forName("com.mysql.jdbc.Driver").newInstance();
                             
                    cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/minor2","root","root");
                    st=cn.createStatement();
                    try{
                         String qry="select id from phone";
                    rs=st.executeQuery(qry);
                    while(rs.next()){
                        phone[i]=rs.getString(1);
                       // out.println(phone[i]);
                        i++;
                    }
                     }
                    
                   catch(Exception e){
                   out.println("1"+e.getMessage());
                   }
                    try{
                    for(i=0;i<884;i++){
                    String q2="select "+col1+" from "+table1+" where id='"+phone[i]+"'";
                   // out.println(q2);
                    rs=st.executeQuery(q2);
                    if(rs.next()){
                    a[i]=rs.getString(1);
                    }
                    }
                    }
                    
                   catch(Exception e){
                   out.println("2"+e.getMessage());
                   }
                    
                    try{
                        for(i=0;i<884;i++){
                    String q3="select "+col2+" from "+table2+" where id='"+phone[i]+"'";
                   // out.println(q3);
                    rs=st.executeQuery(q3);
                    if(rs.next()){
                    b[i]=rs.getString(1);
                    }
                    }
                        }
                    
                   catch(Exception e){
                   out.println("3"+e.getMessage());
                   }
                    try{
                     for(i=0;i<884;i++){
                    String q4="select "+col3+" from "+table3+" where id='"+phone[i]+"'";
                    //out.println(q4);
                    rs=st.executeQuery(q4);
                    if(rs.next()){
                    c[i]=rs.getString(1);
                    }
                    }
                     
                    for(i=0;i<884;i++){
                        if(a[i].contains(val1)){
                            matrix[0][i]=1;
                            
                        }
                        else{
                        matrix[0][i]=0;
                        }                       
                                                                    
                        if(b[i].contains(val2)){
                        matrix[1][i]=1;
                        }
                        else{
                        matrix[1][i]=0;
                        }
                        if(c[i].contains(val3)){
                        matrix[2][i]=1;
                        }
                        else{
                        matrix[2][i]=0;
                        }
                }
                   int[] sum=new int[884];
                   int[] temp= new int[884];
                   int max=0;
                   //int index=0;
                   i=0;
                 //  out.println("now");
                   for(i=0;i<884;i++){
                   sum[i]=matrix[0][i]+matrix[1][i]+matrix[2][i];
                   //out.println("hey");
                 // out.println(i+"->"+sum[i]+",");
                   temp[i]=sum[i];
                   
                   }
                   int z=0;
                   int[] index=new int[5];
                   int ind=0;
                   while( z<5){
                       max=0;
                       ind=0;
                   for(i=0;i<884;i++){
                   if(temp[i]>max) {
                       max=temp[i];
                       ind=i;
                   }
                   }
                   index[z]=ind;
                   temp[ind]=0;
                  // out.println("z"+phone[ind]+"->"+ind+"."+z);
                   z++;

                   }
                   z=0;
      out.write("\n");
      out.write("                   <ul>\n");
      out.write("            ");

                   while(z<5){
                       
                       ResultSet rs1=null;
                   String sq="select name from phone where id='"+phone[index[z]]+"'";
                   rs1=st.executeQuery(sq);
                   if(rs1.next()){
                       
      out.write("\n");
      out.write("                      <li><a href=\"specs.jsp?mbname=");
      out.print(rs1.getString(1));
      out.write("\" onclick=\"location.href=this.href+'&username='+x;return false;\">\n");
      out.write("            \n");
      out.write("            ");

                    out.println("\n"+rs1.getString(1));
      out.write("\n");
      out.write("                          </a>    </li>\n");
      out.write("                          ");

                   }
                   z++;
 
                  
                 //  rs1.close();
                   
                  // }
             
                    }
      out.write("\n");
      out.write("                   </ul>    \n");
      out.write("                      ");

                                     }catch(Exception e){
                   
                        out.println("7"+e.getMessage());
                   }
                                       }
                                     //  }
         
      out.write("\n");
      out.write("             \n");
      out.write("              \n");
      out.write("        </div>\n");
      out.write("        <div class=\"cl\">&nbsp;</div>\n");
      out.write("      </section>\n");
      out.write(" \n");
      out.write("      <section class=\"partners\">\n");
      out.write("        <div id=\"partners-slider\">\n");
      out.write("          <div class=\"slider-holder2\"> <img src=\"css/images/a.jpg\" alt=\"\"> <img src=\"css/images/b.jpg\" alt=\"\"> <img src=\"css/images/c.jpg\" alt=\"\"> <img src=\"css/images/d.jpg\" alt=\"\"> <img src=\"css/images/a.jpg\" alt=\"\"> <img src=\"css/images/b.jpg\" alt=\"\"> <img src=\"css/images/c.jpg\" alt=\"\"> <img src=\"css/images/d.jpg\" alt=\"\"> </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"slider-arr\"> <a class=\"prev-arr arr-btn\" href=\"#\"></a> <a class=\"next-arr arr-btn\" href=\"#\"></a> </div>\n");
      out.write("      </section>\n");
      out.write("    <div class=\"socials\">\n");
      out.write("            <p>We are currently <strong>available</strong> for work.</p>\n");
      out.write("            <ul>\n");
      out.write("              <li><a href=\"https://www.facebook.com/\" class=\"facebook-ico\">facebook-ico</a></li>\n");
      out.write("              <li><a href=\"https://twitter.com/\" class=\"twitter-ico\">twitter-ico</a></li>\n");
      out.write("              <li><a href=\"http://www.skype.com/en/\" class=\"skype-ico\">skype-ico</a></li>\n");
      out.write("              <li><a href=\"http://www.phonearena.com/\" class=\"rss-ico\">rss-ico</a></li>\n");
      out.write("            </ul>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- end of main -->\n");
      out.write("      </div>\n");
      out.write("      <!-- end of shell -->\n");
      out.write("      <!-- footer -->\n");
      out.write("      <div id=\"footer\">\n");
      out.write("        <!-- shell -->\n");
      out.write("        <div class=\"shell\">\n");
      out.write("          <!-- footer-cols -->\n");
      out.write("          <div class=\"footer-cols\">\n");
      out.write("\n");
      out.write("            <div class=\"col\">\n");
      out.write("              <h2>CONTACT us</h2>\n");
      out.write("              <p>Email: <a href=\"#\">addpminor@gmail.com</a></p>\n");
      out.write("              <p>Phone: 09899333927</p>\n");
      out.write("              <p>Address: Jaypee Institue of Information Techonology</p>\n");
      out.write("              <p>Noida</p>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("          <!-- end of footer-cols -->\n");
      out.write("          <div class=\"footer-bottom\">\n");
      out.write("            <div class=\"footer-nav\">\n");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                <li><a hrerf=\"home.jsp\">Home</a></li>\n");
      out.write("\n");
      out.write("                <li><a hrerf=\"www.twitter.com/gadgetaddp\">Twitter@gadgetaddp</a></li>\n");
      out.write("                <li><a hrerf=\"contactus.jsp\">Contacts</a></li>\n");
      out.write("              </ul>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <!--<p class=\"copy\">Copyright &copy; 2012<span>|</span>Design by: <a target=\"_blank\">ChocoTemplates</a></p>-->\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- end of shell -->\n");
      out.write("      </div>\n");
      out.write("      <!-- footer -->\n");
      out.write("    </div>\n");
      out.write("    <!-- end of wrapper -->\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
