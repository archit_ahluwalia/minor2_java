package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import webservice.rating_1;
import webservice.rating_1;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.sql.*;

public final class specs_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

 String[] battery=new String[4],built=new String[4],camera=new String[6],
                connec=new String[6],data=new String[4],display=new String[4]
                ,media=new String[5],memory=new String[2],msg=new String[3],network=new String[1]
                ,software=new String[1];
        String name=null;
                     
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <title>Specifications</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"rating/jquery.rating.css\">\n");
      out.write("                <script src=\"rating/jquery.js\"></script>\n");
      out.write("                <script src=\"rating/jquery.rating.js\"></script>\n");
      out.write("        ");
      out.write("\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\">\n");
      out.write("        <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"css/images/favicon.ico\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"all\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css1/css/style.css\" />\n");
      out.write("        <link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>\n");
      out.write("        <link href='http://fonts.googleapis.com/css?family=Jura:400,500,600,300' rel='stylesheet' type='text/css'>\n");
      out.write("        <script src=\"js/jquery-1.8.0.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"JS1/JS/jquery-1.4.2.min.js\"></script>\n");
      out.write("        <script src=\"JS1/JS/jquery.autocomplete.js\"></script>\n");
      out.write("        <script>\n");
      out.write("        jQuery(function(){\n");
      out.write("        $(\"#mbname1\").autocomplete(\"list.jsp\");\n");
      out.write("        });\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("\n");
      out.write("        <script src=\"js/jquery.touchwipe.1.1.1.js\"></script>\n");
      out.write("        <script src=\"js/jquery.carouFredSel-5.5.0-packed.js\"></script>\n");
      out.write("        <!--[if lt IE 9]><script src=\"js/modernizr.custom.js\"></script><![endif]-->\n");
      out.write("        <script src=\"js/functions.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div id=\"fb-root\"></div>\n");
      out.write("        <script>\n");
      out.write("            var x;\n");
      out.write("            var id;\n");
      out.write("            window.fbAsyncInit = function() {\n");
      out.write("            FB.init({\n");
      out.write("              appId      : '735114853176261',\n");
      out.write("              status     : true, // check login status\n");
      out.write("              cookie     : true, // enable cookies to allow the server to access the session\n");
      out.write("              xfbml      : true  // parse XFBML\n");
      out.write("            });\n");
      out.write("            FB.Event.subscribe('auth.authResponseChange', function(response) {\n");
      out.write("                  if (response.status === 'connected') {\n");
      out.write("                    testAPI();\n");
      out.write("                  } else if (response.status === 'not_authorized') {\n");
      out.write("                    FB.login();\n");
      out.write("                  } else {\n");
      out.write("                    FB.login();\n");
      out.write("                  }\n");
      out.write("                });\n");
      out.write("            };\n");
      out.write("\n");
      out.write("            // Load the SDK asynchronously\n");
      out.write("            (function(d){\n");
      out.write("              var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];\n");
      out.write("              if (d.getElementById(id)) {return;}\n");
      out.write("              js = d.createElement('script'); js.id = id; js.async = true;\n");
      out.write("              js.src = \"//connect.facebook.net/en_US/all.js\";\n");
      out.write("              ref.parentNode.insertBefore(js, ref);\n");
      out.write("             }(document));\n");
      out.write("\n");
      out.write("            function testAPI() {\n");
      out.write("              console.log('Welcome!  Fetching your information.... ');\n");
      out.write("              FB.api('/me', function(response) {\n");
      out.write("                  x=response.name;\n");
      out.write("                  id=response.userID;\n");
      out.write("                  console.log('Good to see you, ' + response.name + '.');\n");
      out.write("                               var y=document.getElementById(\"fbname\");\n");
      out.write("                              y.setAttribute(\"value\",x);\n");
      out.write("              });\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("    <!-- wrapper -->\n");
      out.write("    <div class=\"wrapper\">\n");
      out.write("      <!-- header -->\n");
      out.write("      <header class=\"header\">\n");
      out.write("        <div class=\"shell\">\n");
      out.write("          <div class=\"header-top\">\n");
      out.write("            <h1 id=\"logo\"><a href=\"#\">Gadget Review</a></h1>\n");
      out.write("            <nav id=\"navigation\"> <a href=\"#\" class=\"nav-btn\">Home<span></span></a>\n");
      out.write("              <ul>\n");
      out.write("                <li class=\"active home\">\n");
      out.write("                    <a href=\"home.jsp\">Home</a></li>\n");
      out.write("                <li><a href=\"www.twitter.com/gadgetaddp\">Twitter@gadgetaddp</a></li>\n");
      out.write("                <li><a href=\"contactus.jsp\">Contacts</a></li>\n");
      out.write("              </ul>\n");
      out.write("            </nav>\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("         <div class=\"slider\">\n");
      out.write("            <div id=\"bg\"></div>\n");
      out.write("            <div id=\"carousel\">\n");
      out.write("              <div>\n");
      out.write("                <h3>SPECIFICATIONS</h3>\n");
      out.write("                <p>Enter the phone you like and see its specifications</p>\n");
      out.write("                <img class=\"img-front\" src=\"css/images/front-img.png\" alt=\"\"> \n");
      out.write("                <img class=\"img-mid\" src=\"css/images/img-mid.png\" alt=\"\"> \n");
      out.write("                <img class=\"img-back\" src=\"css/images/img-back.png\" alt=\"\">\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("         </div>\n");
      out.write("     </header>\n");
      out.write("      <!-- end of header -->\n");
      out.write("      <!-- shell -->\n");
      out.write("      <div class=\"shell\">\n");
      out.write("        <!-- main -->\n");
      out.write("        <div class=\"main\">\n");
      out.write("          <!-- cols -->\n");
      out.write("          <section class=\"cols\" id=\"specs\">\n");
      out.write("\n");
      out.write("              <form action=\"specs.jsp#specs\" method=\"get\" name=\"mobs\">\n");
      out.write("                       \n");
      out.write("                  <h2>MOBILE NAME: <input type=\"text\" name=\"mbname\" id=\"mbname1\" />\n");
      out.write("                                   <input class=\"buttonprashi\" type=\"submit\" name=\"mobsubmit\" value=\"Find\"/>\n");
      out.write("                                   <input type=\"hidden\" id=\"fbname\" name=\"username\">\n");
      out.write("                  </h2>\n");
      out.write("              </form>\n");
      out.write("              ");
 
              String name=null;
                 if (request.getParameter("mbname")!=null)
                               {
                                name=request.getParameter("mbname");
                 try {
                    File file = new File("C:/xampp1/htdocs/youcrawl/a.txt");
                            if (!file.exists()) {
                                    file.createNewFile();
                            }

                            FileWriter fw = new FileWriter(file.getAbsoluteFile());
                            BufferedWriter bw = new BufferedWriter(fw);
                            String prashi=name+" review";
                            bw.write(prashi);
                            bw.close();
                            System.out.println("Done");
                    } catch (IOException e) {
                            e.printStackTrace();
                    }
                  }
                           else{
                           
                           }
                 
      out.write("\n");
      out.write("\n");
      out.write("           <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </section>\n");
      out.write("                   ");
 
                    String id=null;
                    int view;
                    view=0;
                    Connection cn;
                    Statement st;

                    Class.forName("com.mysql.jdbc.Driver");
                    cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/minor2","root","root");
                    st=cn.createStatement();
                    if (request.getParameter("mbname")!=null && request.getParameter("mbname")!="")
                    {
                      String mbname=request.getParameter("mbname");
    
      out.write("\n");
      out.write("          <!-- end of cols -->\n");
      out.write("      <section class=\"post\"> <!--<img src=\"css/images/a.jpg\" alt=\"\"> phone pic-->\n");
      out.write("            <div class=\"post-cnt\" >\n");
      out.write("\n");
      out.write("\n");
      out.write("     ");

                       try{

                        String qry="select id,view from phone where name='"+mbname+"'";
                        ResultSet rs=st.executeQuery(qry);
                        if(rs.next())
                        {
                            id=rs.getString(1);
                            view=rs.getInt(2);

                        }
                      rs.close();
                      String sql="update phone set view='"+view+"'+1 where id='"+id+"'";
                      int res1=st.executeUpdate(sql);
                       }
                   catch(Exception ex)
                    {
                        out.println(ex);
                    }

                       try{
                        String qry1="select * from battery where id='"+id+"'";
                        ResultSet rs1=st.executeQuery(qry1);
                        if(rs1.next())
                        {
                            battery[0]=rs1.getString(1);//type
                            battery[1]=rs1.getString(2);//capacity
                            battery[2]=rs1.getString(3);//standby
                            battery[3]=rs1.getString(4);//talktime
                        }
                        rs1.close();
                        //built
                        String qry2="select * from built where id='"+id+"'";
                        rs1=st.executeQuery(qry2);
                        if(rs1.next())
                        {
                            built[0]=rs1.getString(1); //diemansion
                            built[1]=rs1.getString(2);//weight
                            built[2]=rs1.getString(3);//formfactor
                            built[3]=rs1.getString(4);//colors
                        }
                        rs1.close();
                                            //camera
                        String qry3="select * from camera where id='"+id+"'";
                        rs1=st.executeQuery(qry3);
                        if(rs1.next())
                        {
                            camera[0]=rs1.getString(1); //camera
                            camera[1]=rs1.getString(2);//resolutiom
                            camera[2]=rs1.getString(3);//zoom
                            camera[3]=rs1.getString(4);//flash
                            camera[4]=rs1.getString(5);//secondary
                            camera[5]=rs1.getString(6);//videoout
                        }
                        rs1.close();
                        //connectivity
                        String qry4="select * from connectivity where id='"+id+"'";
                        rs1=st.executeQuery(qry4);
                        if(rs1.next())
                        {
                            connec[0]=rs1.getString(1); //bt
                            connec[1]=rs1.getString(2);//irda
                            connec[2]=rs1.getString(3);//wlan
                            connec[3]=rs1.getString(4);//usb
                            connec[4]=rs1.getString(5);//gps

                        }
                        rs1.close();

                        //data
                          String qry5="select * from data where id='"+id+"'";
                        rs1=st.executeQuery(qry5);
                        if(rs1.next())
                        {
                            data[0]=rs1.getString(1); //gprs
                            data[1]=rs1.getString(2);//edge
                            data[2]=rs1.getString(3);//3g
                            data[3]=rs1.getString(4);//internet

                        }
                        rs1.close();  
                        //display
                        String qry6="select * from display where id='"+id+"'";
                        rs1=st.executeQuery(qry6);
                        if(rs1.next())
                        {
                            display[0]=rs1.getString(1); //size
                            display[1]=rs1.getString(2);//type
                            display[2]=rs1.getString(3);//color
                            display[3]=rs1.getString(4);//secondary display

                        }
                        rs1.close();

                         //media
                        String qry7="select * from media where id='"+id+"'";
                        rs1=st.executeQuery(qry7);
                        if(rs1.next())
                        {
                            media[0]=rs1.getString(1); //audioplayback
                            media[1]=rs1.getString(2);//videoplayback
                            media[2]=rs1.getString(3);//ringtone
                            media[3]=rs1.getString(4);//fmradio
                            media[4]=rs1.getString(5);//3.5 mm jack

                        }
                        rs1.close();

                         //memory
                        String qry8="select * from memory where id='"+id+"'";
                        rs1=st.executeQuery(qry8);
                        if(rs1.next())
                        {
                            memory[0]=rs1.getString(1); //inbuilt
                            memory[1]=rs1.getString(2);//slot


                        }
                        rs1.close();

                         //msg
                        String qry9="select * from messaging where id='"+id+"'";
                        rs1=st.executeQuery(qry9);
                        if(rs1.next())
                        {
                            msg[0]=rs1.getString(1); //sms
                            msg[1]=rs1.getString(2);//mms
                            msg[2]=rs1.getString(3);//email
                        }
                        rs1.close();
                        //network
                        String qry10="select * from network where id='"+id+"'";
                        rs1=st.executeQuery(qry10);
                        if(rs1.next())
                        {
                            network[0]=rs1.getString(1); //technology

                        }
                        rs1.close();

                         //software
                        String qry11="select * from software where id='"+id+"'";
                        rs1=st.executeQuery(qry11);
                        if(rs1.next())
                        {
                            software[0]=rs1.getString(1); //os

                        }
                        rs1.close();
                             }  

                    catch(Exception ex)
                    {
                        out.println(ex);
                    }

     
      out.write("\n");
      out.write("           \n");
      out.write("                  <h2 style=\"text-align:center\"> ");
 
                        out.println(request.getParameter("mbname"));
                  
      out.write("  </h2>\n");
      out.write("                 \n");
      out.write("                    <form action=\"specs.jsp#specs\" method=\"post\">\n");
      out.write("                            <input type=\"radio\" name=\"rating\" value=\"1\" class=\"star\">\n");
      out.write("                            <input type=\"radio\" name=\"rating\" value=\"2\" class=\"star\">\n");
      out.write("                            <input type=\"radio\" name=\"rating\" value=\"3\" class=\"star\">\n");
      out.write("                            <input type=\"radio\" name=\"rating\" value=\"4\" class=\"star\">\n");
      out.write("                            <input type=\"radio\" name=\"rating\" value=\"5\" class=\"star\">\n");
      out.write("                            <input type=\"hidden\" id=\"fbname\" name=\"username\" value=\"");
      out.print(request.getParameter("username"));
      out.write("\">\n");
      out.write("                            ");
 if ( request.getParameter("username")!=null||request.getParameter("username").isEmpty()){
                           
                        
      out.write("\n");
      out.write("                            <input type=\"submit\" value=\"");
      out.print(name);
      out.write("\" name=\"mbname\">\n");
      out.write("                            ");

 }                            
                            
      out.write("\n");
      out.write("                    </form>\n");
      out.write("               <a href=\"chartform.java\">view ratings</a>\n");
      out.write("            ");

                     String rating=null;
                     if(request.getParameter("rating")!=null ){
                         rating=request.getParameter("rating");
                     try{
                        String qry="insert into rating(phone_id,user_id,rating) values('"+id+"','"+request.getParameter("username")+"','"+rating+"')";
                        int res=st.executeUpdate(qry);
                        int v=view;
                        String sql="update phone set view='"+v+"' where id='"+id+"'";
                         int res1=st.executeUpdate(sql);
                         }
                      catch(Exception ex)
                         {
                             out.println(ex);
                         }
                     }

            
      out.write("\n");
      out.write("\n");
      out.write("                  <table border=\"1\" cellpadding=\"4\" style=\"width:750px;margin:auto;border:1px solid black;text-align:center;color:black;\">\n");
      out.write("                 <tr>\n");
      out.write("                    <th rowspan=\"4\">BATTERY</th>\n");
      out.write("                        <td>TYPE:</td> <td>");
out.println("\n"+battery[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>CAPACITY</td> <td>");
out.println("\n"+battery[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>STANDBY</td> <td>");
out.println("\n"+battery[2]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>TALKTIME</td> <td>");
out.println("\n"+battery[3]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"4\">BUILT</th>\n");
      out.write("                        <td>DIMENSIONS:</td> <td>");
out.println("\n"+built[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>WEIGHT</td> <td>");
out.println("\n"+built[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>FORMFACTOR</td> <td>");
out.println("\n"+built[2]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>COLORS</td> <td>");
out.println("\n"+built[3]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"6\">CAMERA</th>\n");
      out.write("                        <td>CAMERA:</td> <td>");
out.println("\n"+camera[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>RESOLUTION</td> <td>");
out.println("\n"+camera[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>ZOOM</td> <td>");
out.println("\n"+camera[2]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>FLASH</td> <td>");
out.println("\n"+camera[3]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>SECONDARY_CAMERA</td> <td>");
out.println("\n"+camera[4]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>VIDEO-OUT</td> <td>");
out.println("\n"+camera[5]);
      out.write("</td>\n");
      out.write("                </tr>    \n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"5\">CONNECTIVITY</th>\n");
      out.write("                        <td>BLUETOOTH:</td> <td>");
out.println("\n"+connec[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>IRDA</td> <td>");
out.println("\n"+connec[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>WLAN</td> <td>");
out.println("\n"+connec[2]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>USB</td> <td>");
out.println("\n"+connec[3]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>GPS</td> <td>");
out.println("\n"+connec[4]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"4\">DATA</th>\n");
      out.write("                        <td>GPRS:</td> <td>");
out.println("\n"+data[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>EDGE</td> <td>");
out.println("\n"+data[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>3G</td> <td>");
out.println("\n"+data[2]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>INTERNET_BROWSING</td> <td>");
out.println("\n"+data[3]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"4\">DISPLAY</th>\n");
      out.write("                        <td>SIZE:</td> <td>");
out.println("\n"+display[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>TYPE</td> <td>");
out.println("\n"+display[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>COLORS</td> <td>");
out.println("\n"+display[2]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>SECONDARY_DISPLAY</td> <td>");
out.println("\n"+display[3]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"5\">MEDIA</th>\n");
      out.write("                        <td>AUDIO_PLAYBACK:</td> <td>");
out.println("\n"+media[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>VIDEO_PLAYBACK</td> <td>");
out.println("\n"+media[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>RINGTONES</td> <td>");
out.println("\n"+media[2]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>FM-RADIO</td> <td>");
out.println("\n"+media[3]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                         <td>3.5 MM JACK</td> <td>");
out.println("\n"+media[4]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"2\">MEMORY</th>\n");
      out.write("                        <td>INBUILT:</td> <td>");
out.println("\n"+memory[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>MEMORY SLOT</td> <td>");
out.println("\n"+memory[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"3\">MESSAGING</th>\n");
      out.write("                        <td>SMS:</td> <td>");
out.println("\n"+msg[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>MMS</td> <td>");
out.println("\n"+msg[1]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                     <td>EMAIL</td> <td>");
out.println("\n"+msg[2]);
      out.write("</td> \n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"1\">NETWORK</th>\n");
      out.write("                        <td>TECHNOLOGY:</td> <td>");
out.println("\n"+network[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <th rowspan=\"1\">SOFTWARE</th>\n");
      out.write("                        <td>OPERATING SYSTEM:</td> <td>");
out.println("\n"+software[0]);
      out.write("</td>\n");
      out.write("                </tr>\n");
      out.write("       </table>\n");
      out.write("                <p> <center>\n");
      out.write("                             <a href=\"http://localhost/youcrawl/y41.php\">\n");
      out.write("                       \n");
      out.write("                        <h2>Click Here For Youtube Reviews of The Phone\n");
      out.write("                        </h2>\n");
      out.write("                                   </a> \n");
      out.write("                   </center>\n");
      out.write("                           \n");
      out.write("                </p>\n");
      out.write("                <p> <center>\n");
      out.write("                             <a href=\"http://localhost:8080/minor/index.jsp?mbname=");
      out.print(mbname);
      out.write("\">\n");
      out.write("                       \n");
      out.write("                        <h2>For Reviews Analysis Click here\n");
      out.write("                        </h2>\n");
      out.write("                                   </a> \n");
      out.write("                   </center>\n");
      out.write("                           \n");
      out.write("                </p>\n");
      out.write("                 \n");
      out.write("            </div>\n");
      out.write("            <div class=\"cl\" id=\"comments\">&nbsp;</div>\n");
      out.write("          </section>\n");
      out.write("                    \n");
      out.write("          <section class=\"content\" id=\"rating\">\n");
      out.write("           \n");
      out.write("              <table width=\"100%\"><tr><td width=\"100%\">\n");
      out.write("                <h2 style=\"text-align:center\">RATINGS CORNER</h2>\n");
      out.write("                </td></tr>\n");
      out.write("              </table>\n");
      out.write("            \n");
      out.write("                      \n");
      out.write("                  ");

                rating_1 r=new rating_1();
                 List <Integer> battery=null;
                 battery=r.getClientRating(id, "battery");
                 List <Integer> display=null;
                 display=r.getClientRating(id, "display");
                 List <Integer> camera=null;
                 camera=r.getClientRating(id, "camera");
                   int brd=battery.get(1);
                int crd=camera.get(1);
                int drd=display.get(1);
                int bcd=battery.get(0);
                int ccd=camera.get(0);
                int dcd=display.get(0); 
                if(ccd==0){
                ccd=1;
}
                 if(dcd==0){
                ccd=1;
}
                 if(bcd==0){
                ccd=1;
}

      out.write("\n");
      out.write("             \n");
      out.write("               \n");
      out.write("              <table width=\"100%\">\n");
      out.write("                  <th><td style=\"text-align:center\"><h3> Average Ratings</h3></td></th>\n");
      out.write("                    <tr>\n");
      out.write("                        <td width=\"33%\" style=\"text-align:center\">\n");
      out.write("                            Battery :");

                            float bat=(brd/bcd);
                            out.println(bat);

      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td width=\"33%\" style=\"text-align:center\"> \n");
      out.write("                            Display :");

                            float dis=(drd/dcd);
                            out.println(dis);

      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td width=\"33%\" style=\"text-align:center\"> \n");
      out.write("                            Camera :");

                            float cam=(crd/ccd);
                            out.println(cam);

      out.write("\n");
      out.write("                        </td>\n");
      out.write("                    \n");
      out.write("                    </tr></table>\n");
      out.write("                    <table width=\"100%\">\n");
      out.write("                        <tr>\n");
      out.write("                        <td width=\"50%\" style=\"text-align:center\">\n");
      out.write("                            OverAll Rating:\n");
      out.write("                            ");

                            try{
                        String qry="select rating from rating where phone_id='"+id+"'";
                        ResultSet rs=st.executeQuery(qry);
                        if(rs.next())
                        {
                           out.println(rs.getInt(1));
                                                     
                        }
                      rs.close();
                        

      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td width=\"50%\" style=\"text-align:center\">\n");
      out.write("                            Total Views:\n");
      out.write("                         ");

                         
                        out.println(r.getClientView(id));
                                             }
                            catch(Exception e){
                                out.println(e);
                            }
    
      out.write("   \n");
      out.write("                        </td>\n");
      out.write("                    </tr>\n");
      out.write("                </table>\n");
      out.write("                        <hr>\n");
      out.write("                         <form action=\"specs.jsp#rating\" method=\"post\">\n");
      out.write("                       ");
    
                       if(request.getParameter("Vote")!="" && request.getParameter("Vote")!=null){
                       
                        try{                     
                        int br=Integer.parseInt(request.getParameter("bRating"));
                        int cr=Integer.parseInt(request.getParameter("cRating"));
                        int dr=Integer.parseInt(request.getParameter("dRating"));
                        r.update(id, "battery", br);
                        r.update(id, "camera", cr);
                        r.update(id,"display",dr);
                        }catch(Exception e){
                         out.println(e);
                         
                         }                    
                    }
     
      out.write("\n");
      out.write("                        <input type=\"hidden\" name=\"mobilename\" value=\"");
      out.print(id);
      out.write("\">\n");
      out.write("                        <input type=\"hidden\" name=\"mbname\" value=\"");
      out.print(name );
      out.write("\">\n");
      out.write("                   <!--    <input type=\"hidden\" name=\"username\" ><!--value=\"");
//=request.getParameter("username")
      out.write("\">-->\n");
      out.write("                       \n");
      out.write("                        <input type=\"hidden\" id=\"fbname\" name=\"username\" value=\"");
      out.print(request.getParameter("username"));
      out.write("\">\n");
      out.write("              <table width=\"100%\">\n");
      out.write("                   <th><td style=\"text-align:center\"><h3> VOTE</h3></td></th>\n");
      out.write("                    <tr>\n");
      out.write("                        <td width=\"33%\" style=\"text-align:center\">\n");
      out.write("                            Battery :<select name=\"bRating\">\n");
      out.write("                                <option value=\"1\">1</option>\n");
      out.write("                                <option value=\"2\">2</option>\n");
      out.write("                                <option value=\"3\">3</option>\n");
      out.write("                                <option value=\"4\">4</option>\n");
      out.write("                                <option value=\"5\">5</option>\n");
      out.write("                            </select>\n");
      out.write("                        </td>\n");
      out.write("                        <td width=\"33%\" style=\"text-align:center\">  \n");
      out.write("                            Display :<select name=\"dRating\">\n");
      out.write("                                 <option value=\"1\">1</option>\n");
      out.write("                                <option value=\"2\">2</option>\n");
      out.write("                                <option value=\"3\">3</option>\n");
      out.write("                                <option value=\"4\">4</option>\n");
      out.write("                                <option value=\"5\">5</option>\n");
      out.write("                            </select></td>\n");
      out.write("                        <td width=\"33%\" style=\"text-align:center\">  \n");
      out.write("                            Camera :<select name=\"cRating\">\n");
      out.write("                                 <option value=\"1\">1</option>\n");
      out.write("                                <option value=\"2\">2</option>\n");
      out.write("                                <option value=\"3\">3</option>\n");
      out.write("                                <option value=\"4\">4</option>\n");
      out.write("                                <option value=\"5\">5</option>\n");
      out.write("                            </select></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr> <td width=\"33%\"></td><td width=\"33%\">\n");
      out.write("                            <br>          <br>\n");
      out.write("                    <center> <input type=\"submit\" name=\"Vote\" ></center></td>\n");
      out.write("                        <td width=\"33%\"> </td>\n");
      out.write("                    </tr>\n");
      out.write("                </table>\n");
      out.write("                         </form>\n");
      out.write("             </section> \n");
      out.write("          <section class=\"partners\" id=\"comment\">\n");
      out.write("                 ");
 out.println(request.getParameter("username"));
      out.write("\n");
      out.write("            <h2 style=\"text-align:center\">YOUR PERSONAL COMMENTS:</h2>\n");
      out.write("                    <fb:login-button show-faces=\"true\" width=\"200\" max-rows=\"1\"></fb:login-button>\n");
      out.write("\n");
      out.write("\n");
      out.write("                    <form action=\"commenttobean.jsp\" name=\"comments\" method=\"post\">\n");
      out.write("                            <textarea rows=\"4\" cols=\"170\" name=\"txtinput\">\n");
      out.write("                            </textarea>\n");
      out.write("                        <input type=\"hidden\" name=\"mobilename\" value=\"");
      out.print(id);
      out.write("\">\n");
      out.write("                        <input type=\"hidden\" name=\"mbname\" value=\"");
      out.print(name );
      out.write("\">\n");
      out.write("                   <!--    <input type=\"hidden\" name=\"username\" ><!--value=\"");
//=request.getParameter("username")
      out.write("\">-->\n");
      out.write("                        <br>\n");
      out.write("                        <input type=\"submit\" name=\"Comment\">\n");
      out.write("                        <input type=\"hidden\" id=\"fbname\" name=\"username\" value=\"");
      out.print(request.getParameter("username"));
      out.write("\">\n");
      out.write("                      </form>\n");
      out.write("               \n");
      out.write("                    \n");
      out.write("                      <div>\n");
      out.write("                                <table width=\"600px\">    \n");
      out.write("\n");
      out.write("          \n");
      out.write("            ");

                  // String[] comment=null,date=null,uname=null;
   try
        {
             ResultSet rs=null;
            String qry1="select * from comment where phone_id='"+id+"' order by date desc";
            st=cn.createStatement();
            rs=st.executeQuery(qry1);
            
            while(rs.next()){
                
                
      out.write("\n");
      out.write("               \n");
      out.write("                <tr><td><table width=\"900px\" height=\"125 px\" style=\"background-color:#CCCCFF\">\n");
      out.write("                            <tr><td  style=\"text-align:left\">");
      out.print(rs.getString("user_id"));
      out.write("</td>\n");
      out.write("                        <td  style=\"text-align:right\">");
      out.print(rs.getString("date"));
      out.write("</td></tr><hr>\n");
      out.write("                    <tr><td colspan=\"2\">");
      out.print(rs.getString("comment1"));
      out.write("</td></tr>\n");
      out.write("                    <hr>\n");
      out.write("                </table></td></tr>\n");
      out.write("                </table> \n");
      out.write("\n");
      out.write("                      \n");
      out.write("                    ");

            }
            //st.close();
        }
        catch(Exception ex)
        {
            System.out.println("Error : " + ex);
        }                   


      out.write('\n');
      out.write(' ');
 }
          
      out.write("\n");
      out.write("          </div>      </section>\n");
      out.write("          <div class=\"socials\">\n");
      out.write("            <p>We are currently <strong>available</strong> for work.</p>\n");
      out.write("            <ul>\n");
      out.write("              <li><a href=\"https://www.facebook.com/\" class=\"facebook-ico\">facebook-ico</a></li>\n");
      out.write("              <li><a href=\"https://twitter.com/\" class=\"twitter-ico\">twitter-ico</a></li>\n");
      out.write("              <li><a href=\"http://www.skype.com/en/\" class=\"skype-ico\">skype-ico</a></li>\n");
      out.write("              <li><a href=\"http://www.phonearena.com/\" class=\"rss-ico\">rss-ico</a></li>\n");
      out.write("            </ul>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- end of main -->\n");
      out.write("      </div>\n");
      out.write("      <!-- end of shell -->\n");
      out.write("      <!-- footer -->\n");
      out.write("      <div id=\"footer\">\n");
      out.write("        <!-- shell -->\n");
      out.write("        <div class=\"shell\">\n");
      out.write("          <!-- footer-cols -->\n");
      out.write("          <div class=\"footer-cols\">\n");
      out.write("\n");
      out.write("            <div class=\"col\">\n");
      out.write("              <h2>CONTACT us</h2>\n");
      out.write("              <p>Email: <a href=\"#\">addpminor@gmail.com</a></p>\n");
      out.write("              <p>Phone: 09899333927</p>\n");
      out.write("              <p>Address: Jaypee Institue of Information Techonology</p>\n");
      out.write("              <p>Noida</p>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("          <!-- end of footer-cols -->\n");
      out.write("          <div class=\"footer-bottom\">\n");
      out.write("            <div class=\"footer-nav\">\n");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                <li><a hrerf=\"home.jsp\">Home</a></li>\n");
      out.write("\n");
      out.write("                <li><a hrerf=\"www.twitter.com/gadgetaddp\">Twitter@gadgetaddp</a></li>\n");
      out.write("                <li><a hrerf=\"contactus.jsp\">Contacts</a></li>\n");
      out.write("              </ul>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <!--<p class=\"copy\">Copyright &copy; 2012<span>|</span>Design by: <a target=\"_blank\">ChocoTemplates</a></p>-->\n");
      out.write("            <div class=\"cl\">&nbsp;</div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- end of shell -->\n");
      out.write("      </div>\n");
      out.write("      <!-- footer -->\n");
      out.write("    </div>\n");
      out.write("    <!-- end of wrapper -->\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
