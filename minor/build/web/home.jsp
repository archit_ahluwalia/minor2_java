<%@page import="org.jsoup.nodes.Element"%>
<%@page import="org.jsoup.nodes.Document"%>
<%@page import="org.jsoup.Jsoup"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Gadget Analysis</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Jura:400,500,600,300' rel='stylesheet' type='text/css'>
<script src="js/jquery-1.8.0.min.js"></script>
<script src="js/jquery.touchwipe.1.1.1.js"></script>
<script src="js/jquery.carouFredSel-5.5.0-packed.js"></script>
<!--[if lt IE 9]><script src="js/modernizr.custom.js"></script><![endif]-->
<script src="js/functions.js"></script>
</head>
<body>
     <script>
        var x;
        var id;
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '735114853176261',
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

    FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
       
      
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        x=response.name;
        id=response.userID;
      console.log('Good to see you, ' + response.name + '.');
      //document.write("hey"+response.name);
      // elem = document.getElementById("demo");
        //            elem.innerHTML = x;
          //          elem2=document.getElementByid("forid");
            //        elem2.innerHTML=id;
                   // var y=document.createElement("INPUT");
                     var y=document.getElementById("fbname");
                    y.setAttribute("value",x);
    });
  }
</script>
<!-- wrapper -->
<div class="wrapper">
  <!-- header -->
  <header class="header">
    <div class="shell">
      <div class="header-top">
        <h1 id="logo"><a href="#">Gadget Review</a></h1>
        <nav id="navigation"> <a href="#" class="nav-btn">Home<span></span></a>
          <ul>
            <li class="active home"><a href="home.jsp">Home</a></li>
            <li><a href="https://twitter.com/gadgetaddp">Twitter@gadgetaddp</a></li>
            <li><a href="http://localhost:8080/contactus-war/">Contact Us</a></li>
          </ul>
        </nav>
        <div class="cl">&nbsp;</div>
      </div>
     <div class="slider">
        <div id="bg"></div>
        <div id="carousel">
          <div>
            <h3>Specifications</h3>
            <p>To get the specifications of the image page</p>
            <a href="specs.jsp" class="green-btn">Specifications</a> <img class="img-front" src="css/images/front-img.png" alt=""> <img class="img-mid" src="css/images/img-mid.png" alt=""> <img class="img-back" src="css/images/img-back.png" alt=""> </div>
          <div>
            <h3>Search on specifications</h3>
            
            <a href="specificsearch.jsp" class="green-btn">Search</a> <img class="img-front" src="css/images/front-img.png" alt=""> <img class="img-mid" src="css/images/img-mid.png" alt=""> <img class="img-back" src="css/images/img-back.png" alt=""> </div>
         
            <div>
            <h3>Compare</h3>
            <p>Compare 2 phones based on the weightage for each category</p>
            <a href="compare.jsp" class="green-btn">Compare</a> <img class="img-front" src="css/images/front-img.png" alt=""> <img class="img-mid" src="css/images/img-mid.png" alt=""> <img class="img-back" src="css/images/img-back.png" alt=""> </div>
          <div>
            <h3>Analysis</h3>
            <p>
			Displays the graph of the most popluar phones and graph based on ratings
			</p>
            <a href="chartt.jsp" class="green-btn">Analysis</a> <img class="img-front" src="css/images/front-img.png" alt=""> <img class="img-mid" src="css/images/img-mid.png" alt=""> <img class="img-back" src="css/images/img-back.png" alt=""> </div>
          <div>
            <h3>Brands</h3>
			<p>Search for the post popular phones of a brand or select a phone as per brand</p>
            <a href="brand.jsp" class="green-btn">Brands</a> <img class="img-front" src="css/images/front-img.png" alt=""> <img class="img-mid" src="css/images/img-mid.png" alt=""> <img class="img-back" src="css/images/img-back.png" alt=""> </div>
        </div>
        <div class="pagination"></div>
        <a id="prev" href="#"></a><br> <a id="next" href="#"></a> </div>
    </div>
  </header>
  <!-- end of header -->
  <!-- shell -->
  <div class="shell">
    <!-- main -->
    <div class="main">
     
           <section class="cols">
        <div class="col">
          <div class="col-cnt">
              <table style="width:300px"><h2 style="text-align:center"> Most Searched Phones</h2>
    
               <% 
                   String id=null;
                    int view;
                    view=0;
                    Connection cn;
                    Statement st;
                   

                    Class.forName("com.mysql.jdbc.Driver");
                    cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/minor2","root","root");
                    st=cn.createStatement();
                    String sql="Select name from phone order by view desc";
                    ResultSet rs=st.executeQuery(sql);
                    int i=0;
     while(rs.next()&& i<10)
                                               {
         %>
         <tr><td style="width:300px;text-align:left">
                <!-- <a href="specs.jsp?mbname=<%=rs.getString("name")%>&username=">-->
                <a href="specs.jsp?mbname=<%=rs.getString("name")%>" onclick="location.href=this.href+'&username='+x;return false;">     
                     
         <%
                    out.println(rs.getString("name"));
                        i++;   
                
                    
%>
                 </a> </td></tr>
         <% }%>
</table> 
           </div>
        </div>
        <div class="col"> 
          <div class="col-cnt">
              <table ><h2 style="text-align:center">LATEST PHONES</h2>
    
               <% 
                  
                    sql="Select name from phone order by date desc";
                    rs=st.executeQuery(sql);
                    i=0;
     while(rs.next()&& i<10)
                                               {
         %>
         <tr><td style="width:300px;text-align:left;list-style: disc">
                <!-- <a href="specs.jsp?mbname=<%=rs.getString("name")%>&username=">-->
  <a href="specs.jsp?mbname=<%=rs.getString("name")%>" onclick="location.href=this.href+'&username='+x;return false;">  
      
         <%
                    out.println(rs.getString("name"));
                        i++;   
                
                    
%>
                 </a> </td></tr>
         <% }%>
</table>
             </div>
        </div>
        <div class="col"> 
          <div class="col-cnt">
              <table style="width:325px;background-color:#FFFFCC"><tr><td style="text-align:center">
                          <h2>TECHNICAL NEWS</h2></td></tr>
               <% 
                   Document doc = Jsoup.connect("http://www.technewsworld.com/perl/section/mobile-tech/").get();
            for(Element doc1 : doc.select("div.content-block table")){
                   
              %>
                      <tr>
                        <td> <ul>
                      <li> <a href="news.jsp?link=<%out.println(doc1.child(0).child(0).child(0).child(0).absUrl("href"));%>">
                                    <%
                    out.println(doc1.child(0).child(0).child(0).child(0).html());
%>
                             </a> </li>   </ul> 
                          </td> 
                  </tr>
                         
         <%
               }    
                    
%>
       
</table>
           </div>
        </div>
        <div class="cl">&nbsp;</div>
      </section>
      <!-- end of cols -->
      <section class="post"> 
        <div class="post-cnt">
          <h2>TWEETS FOR OUR WEBSITES</h2>
          <a align="right" class="twitter-timeline" href="https://twitter.com/gadgetaddp" data-widget-id="448508029739102208">Tweets by @gadgetaddp</a>
<script>
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
</script>
        </div>
        <div class="cl">&nbsp;</div>
      </section>
      <section class="content">
        <h2>WHAT DOES OUR SITE OFFER</h2>
        
        <p> Seeing the  recent growing market for mobiles making a choice becomes difficult. And that is the reason for this site. Based on the user's review 
		provided by user <span>tweets</span> and the <span>youtube vidoes</span> we are going to help the users by giving them a review of their fellow-users. This will help the users
		in making the right decisions and decide on whats best for them.<br>
          <a href="#" class="more">meet the team</a></p>
      </section>
      <section class="partners">
        <div id="partners-slider">
          <div class="slider-holder2"> <img src="css/images/a.jpg" alt=""> <img src="css/images/b.jpg" alt=""> <img src="css/images/c.jpg" alt=""> <img src="css/images/d.jpg" alt=""> <img src="css/images/a.jpg" alt=""> <img src="css/images/b.jpg" alt=""> <img src="css/images/c.jpg" alt=""> <img src="css/images/d.jpg" alt=""> </div>
        </div>
        <div class="slider-arr"> <a class="prev-arr arr-btn" href="#"></a> <a class="next-arr arr-btn" href="#"></a> </div>
      </section>
      <div class="socials">
        <p>We are currently <strong>available</strong> for work.</p>
        <ul>
          <li><a href="https://www.facebook.com/" class="facebook-ico">facebook-ico</a></li>
          <li><a href="https://twitter.com/" class="twitter-ico">twitter-ico</a></li>
          <li><a href="http://www.skype.com/en/" class="skype-ico">skype-ico</a></li>
          <li><a href="http://www.phonearena.com/" class="rss-ico">rss-ico</a></li>
        </ul>
      </div>
    </div>
    <!-- end of main -->
  </div>
  <!-- end of shell -->
  <!-- footer -->
  <div id="footer">
    <!-- shell -->
    <div class="shell">
      <!-- footer-cols -->
      <div class="footer-cols">
        
        
        <div class="col">
          <h2>CONTACT us:</h2>
          <p>Email: <a href="#">addpminor@gmail.com</a></p>
          <p>Phone: 09899333927</p>
          <p>Address: Jaypee Institue of Information Techonology</p>
          <p>Noida</p>
        </div>
        <div class="cl">&nbsp;</div>
      </div>
      <!-- end of footer-cols -->
      <div class="footer-bottom">
        <div class="footer-nav">
          <ul>
            <li><a hrerf="home.jsp">Home</a></li>
            <li><a hrerf="https://twitter.com/gadgetaddp">Twitter</a></li>
            <li><a hrerf="contactus.jsp">Contacts</a></li>
          </ul>
        </div>
        <!--<p class="copy">Copyright &copy; 2012<span>|</span>Design by: <a target="_blank">ChocoTemplates</a></p>-->
        <div class="cl">&nbsp;</div>
      </div>
    </div>
    <!-- end of shell -->
  </div>
  <!-- footer -->
</div>
<!-- end of wrapper -->
</body>
</html>