<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.File"%>

<%@page import="java.sql.*"%>
<%@page import="java.io.IOException"%>

<%@page import="org.jsoup.nodes.Document"%>
<%@page import="org.jsoup.nodes.Element"%>
<%@page import="org.jsoup.Jsoup"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Digy</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Jura:400,500,600,300' rel='stylesheet' type='text/css'>
<script src="js/jquery-1.8.0.min.js"></script>
<script src="js/jquery.touchwipe.1.1.1.js"></script>
<script src="js/jquery.carouFredSel-5.5.0-packed.js"></script>
<!--[if lt IE 9]><script src="js/modernizr.custom.js"></script><![endif]-->
<script src="js/functions.js"></script>
</head>
<body>
<!-- wrapper -->
<div class="wrapper">
      <!-- header -->
      <header class="header">
        <div class="shell">
          <div class="header-top">
            <h1 id="logo"><a href="#">Gadget Review</a></h1>
            <nav id="navigation"> <a href="#" class="nav-btn">Home<span></span></a>
              <ul>
                <li class="active home">
                    <a href="home.jsp">Home</a></li>
                <li><a href="www.twitter.com/gadgetaddp">Twitter@gadgetaddp</a></li>
                <li><a href="contactus.jsp">Contacts</a></li>
              </ul>
            </nav>
            <div class="cl">&nbsp;</div>
          </div>
         <div class="slider">
            <div id="bg"></div>
            <div id="carousel">
              <div>
                <h3>Reviews</h3>
                <p>See the reviews for the phone</p>
                <%if(request.getParameter("mbname")!=null){ %>
                <img class="img-front" src="css/images/front-img.png" alt=""> 
                <img class="img-mid" src="css/images/img-mid.png" alt=""> 
                <img class="img-back" src="css/images/img-back.png" alt="">
              </div>
            </div>
         </div>
     </header>
      <!-- end of header -->
      <!-- shell -->
      <div class="shell">
        <!-- main -->
        <div class="main">
          <!-- cols -->
         
              
      <!-- end of cols -->
      <section class="post"> 
          <h2 style="text-align: center">
              <%
              out.println(request.getParameter("mbname"));
%>
          </h2>
        <div class="post-cnt">
            <h4>
             <% 

    String mbname=request.getParameter("mbname");
    int count,count1=0;
    count=0;
    ArrayList<String> review=new ArrayList();
    //review.add("  ");
Document doc = Jsoup.connect("http://www.cnet.com/search/?query="+mbname).get();
            for(Element doc1 : doc.select("div.result-list section.items section.searchItem")){
                   if(doc1.child(1).child(0).html().equals("Review") && count==0){
                       count+=1;
                       Document newDoc=Jsoup.connect(doc1.child(1).child(1).absUrl("href")+"2").get();
                       //out.println(doc1.child(1).child(1).absUrl("href")+"2");
                       for(Element doc2: newDoc.select("div#editorReview")){
                           
                           //if(doc2.html().contains("Conclusion")){
                               //int count1=0;
                           Element doc3=doc2.child(0).lastElementSibling();
                           while((doc3.hasText()|| count1<4)&& doc3.nodeName()!="figure"){
                               out.println(doc3.html());
                               review.add(doc3.html());
                               doc3=doc3.previousElementSibling();
                               
                              // doc2.lastElementSibling();
                                //while(doc2.nextElementSibling()!=null && doc2.nextSibling().nodeName()!="figure"){
                                 //out.println(doc2.nextElementSibling().html());
                                 count1+=1;
                                 //  out.println(doc2.nextSibling().nodeName());
                                  //  doc2=doc2.nextElementSibling();
                           }
                                //}
                        //   review.add(" ");
                                                                                       //}
                       }
                         
                    }
                   
                   
        }
           


%></h4>
  <%  try {
 		File file = new File("C:/xampp1/htdocs/analysis/a.txt");
 			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
                        //String prashi=sentiment;
			bw.write(review.toString());
			bw.close();
 			System.out.println("Done");
 		} catch (IOException e) {
			e.printStackTrace();
		}
            }
            %>
        </div>
        <div class="cl">&nbsp;</div>
      </section>
      
      <section class="partners">
          <h2 style="text-align: center">
              Sentiment Analysis of Review
          </h2>
          <table width="100%"><tr><td width="33%"> <a href="http://localhost/analysis/knn3.php">K-NN algorithm</a>
                  </td>
                  <td width="33%"><a href="http://localhost/analysis/ownalgo3.php">Own algorithm</a>
                  </td>
                  <td width="33%"><a href="http://localhost/analysis/bayes3.php">bayes3 algorithm</a>
                  </td>
          </table>
                      <p>

      </section>
      <div class="socials">
        <p>We are currently <strong>available</strong> for work. Please, contact us for a quote at <span><a href="#">contact [at] websitename [dot] com</a></span></p>
        <ul>
          <li><a href="#" class="facebook-ico">facebook-ico</a></li>
          <li><a href="#" class="twitter-ico">twitter-ico</a></li>
          <li><a href="#" class="skype-ico">skype-ico</a></li>
          <li><a href="#" class="rss-ico">rss-ico</a></li>
        </ul>
      </div>
    </div>
    <!-- end of main -->
  </div>
  <!-- end of shell -->
  <!-- footer -->
      <div id="footer">
        <!-- shell -->
        <div class="shell">
          <!-- footer-cols -->
          <div class="footer-cols">

            <div class="col">
              <h2>CONTACT us</h2>
              <p>Email: <a href="#">addpminor@gmail.com</a></p>
              <p>Phone: 09899333927</p>
              <p>Address: Jaypee Institue of Information Techonology</p>
              <p>Noida</p>
            </div>
            <div class="cl">&nbsp;</div>
          </div>
          <!-- end of footer-cols -->
          <div class="footer-bottom">
            <div class="footer-nav">

                <ul>
                <li><a hrerf="home.jsp">Home</a></li>

                <li><a hrerf="www.twitter.com/gadgetaddp">Twitter@gadgetaddp</a></li>
                <li><a hrerf="contactus.jsp">Contacts</a></li>
              </ul>

            </div>
            <!--<p class="copy">Copyright &copy; 2012<span>|</span>Design by: <a target="_blank">ChocoTemplates</a></p>-->
            <div class="cl">&nbsp;</div>
          </div>
        </div>
        <!-- end of shell -->
      </div>
  <!-- footer -->
</div>
<!-- end of wrapper -->
</body>
</html>