<%-- 
    Document   : news
    Created on : May 12, 2014, 10:50:09 PM
    Author     : devansh
--%>

<%@page import="org.jsoup.select.Elements"%>
<%@page import="org.jsoup.nodes.Element"%>
<%@page import="org.jsoup.nodes.Document"%>
<%@page import="org.jsoup.Jsoup"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
	<!DOCTYPE html>
<html lang="en">
<head>
<title>Gadget Analysis</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">
<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<link href='http://fonts.googleapis.com/css?family=Coda' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Jura:400,500,600,300' rel='stylesheet' type='text/css'>
<script src="js/jquery-1.8.0.min.js"></script>
<script src="js/jquery.touchwipe.1.1.1.js"></script>
<script src="js/jquery.carouFredSel-5.5.0-packed.js"></script>
<!--[if lt IE 9]><script src="js/modernizr.custom.js"></script><![endif]-->
<script src="js/functions.js"></script>
</head>
<body>
<!-- wrapper -->
 <!-- wrapper -->
    <div class="wrapper">
      <!-- header -->
      <header class="header">
        <div class="shell">
          <div class="header-top">
            <h1 id="logo"><a href="#">Gadget Review</a></h1>
            <nav id="navigation"> <a href="#" class="nav-btn">Home<span></span></a>
              <ul>
                <li class="active home">
                    <a href="home.jsp">Home</a></li>
                <li><a href="www.twitter.com/gadgetaddp">Twitter@gadgetaddp</a></li>
                <li><a href="contactus.jsp">Contacts</a></li>
              </ul>
            </nav>
            <div class="cl">&nbsp;</div>
          </div>
         <div class="slider">
            <div id="bg"></div>
            <div id="carousel">
              <div>
                <h3>News</h3>
                <p>Read News and Stay up to date</p>
                <img class="img-front" src="css/images/front-img.png" alt=""> 
                <img class="img-mid" src="css/images/img-mid.png" alt=""> 
                <img class="img-back" src="css/images/img-back.png" alt="">
              </div>
            </div>
         </div>
     </header>
      <!-- end of header -->
      <!-- shell -->
      
      <div class="shell">
          </br>
        <!-- main -->
        <%
        String link=request.getParameter("link");
            Document doc = Jsoup.connect(link).get();
            for(Element doc1:doc.select("div#story")){
            //for(Element doc1 : doc.select("div.content-block table")){
                 out.println(doc1.getElementsByClass("title"));
                 out.println(doc1.getElementsByClass("story-byline"));
        %>
        
        <div class="main">
              <section class="cols" id="specs">
         
          
          <!-- cols -->
        <% 
            
                 Elements doc2=doc1.getElementsByClass("story-body");
                 for(Element d:doc2){
                     Element doc3=d;
                       do{
                           if(doc3.hasClass("subhead")){
                           %>
                           <h3>
        <%
                               out.println(doc3.html());
        %>
                           </h3>
                               <%
                           }
                           else{
                                                          out.println(doc3.html());

                           }
                           doc3=doc3.nextElementSibling();
                           %>
                           <p>
        <%
                           
                 }while(doc3.nextElementSibling()!=null);
                 }
                 


              
  }    
                    
%>
  
        
        </div>
      </div> 
       <div class="cl">&nbsp;</div>
      </section>
      <!-- end of cols -->
      <section class="post"> 
        <div class="post-cnt">
         
           
        </div>
        <div class="cl">&nbsp;</div>
      </section>
      <section class="content">
       
		
		</section>
		<section class="partners">
        <div id="partners-slider">
          <div class="slider-holder2"> <img src="css/images/a.jpg" alt=""> <img src="css/images/b.jpg" alt=""> <img src="css/images/c.jpg" alt=""> <img src="css/images/d.jpg" alt=""> <img src="css/images/a.jpg" alt=""> <img src="css/images/b.jpg" alt=""> <img src="css/images/c.jpg" alt=""> <img src="css/images/d.jpg" alt=""> </div>
        </div>
        <div class="slider-arr"> <a class="prev-arr arr-btn" href="#"></a> <a class="next-arr arr-btn" href="#"></a> </div>
      </section>
     <div class="socials">
            <p>We are currently <strong>available</strong> for work.</p>
            <ul>
              <li><a href="https://www.facebook.com/" class="facebook-ico">facebook-ico</a></li>
              <li><a href="https://twitter.com/" class="twitter-ico">twitter-ico</a></li>
              <li><a href="http://www.skype.com/en/" class="skype-ico">skype-ico</a></li>
              <li><a href="http://www.phonearena.com/" class="rss-ico">rss-ico</a></li>
            </ul>
          </div>
        </div>
        <!-- end of main -->
      </div>
      <!-- end of shell -->
      <!-- footer -->
      <div id="footer">
        <!-- shell -->
        <div class="shell">
          <!-- footer-cols -->
          <div class="footer-cols">

            <div class="col">
              <h2>CONTACT us</h2>
              <p>Email: <a href="#">addpminor@gmail.com</a></p>
              <p>Phone: 09899333927</p>
              <p>Address: Jaypee Institue of Information Techonology</p>
              <p>Noida</p>
            </div>
            <div class="cl">&nbsp;</div>
          </div>
          <!-- end of footer-cols -->
          <div class="footer-bottom">
            <div class="footer-nav">

               

            </div>
            <!--<p class="copy">Copyright &copy; 2012<span>|</span>Design by: <a target="_blank">ChocoTemplates</a></p>-->
            <div class="cl">&nbsp;</div>
          </div>
        </div>
        <!-- end of shell -->
      </div>
      <!-- footer -->
    </div>
    <!-- end of wrapper -->
    </body>
</html>