
import java.io.File;
import java.io.IOException;
import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jfree.chart.*;
import org.jfree.chart.entity.*;
import org.jfree.chart.plot.*;
import org.jfree.data.category.*;
@WebServlet(name = "chartform", urlPatterns = {"/chartform"})
public class chartform extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
     
           Connection cn=null;
                    Statement st=null;
                    int i=0;
                    ResultSet rs;
                     
                    try{
                     Class.forName("com.mysql.jdbc.Driver");
                    cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/minor2","root","root");
                    st=cn.createStatement();
                   
                    }
                    catch(Exception e){
                    System.out.println(e.getMessage());
                    }
                    
                    if("View Analysis".equals(request.getParameter("type")))
                    {
                        try{
                             DefaultCategoryDataset  categoryDataset = new DefaultCategoryDataset();
                     String name[]=new String[5],view[]=new String[5];
                          
                    String sql="select name,view from phone order by view desc";
                    rs=st.executeQuery(sql);
                    while(rs.next() && i<5){
                    name[i]=rs.getString(1);
                    view[i]=rs.getString(2);
                    categoryDataset.setValue(Integer.parseInt(view[i]), "phone view", name[i]);
                    i++;
                    }
                    rs.close();
                          
        
            JFreeChart chart = ChartFactory.createBarChart3D
                     ("Views", // Title
                      "name",              // X-Axis label
                      "view",// Y-Axis label
                      categoryDataset,         // Dataset
                      PlotOrientation.VERTICAL,
                      true,                     // Show legend
                      true,
                      false
                     );
             
            
                final ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
                final File file = new File("C:/Users/Archit/Documents/NetBeansProjects/minor/minor/web/charts/barchart3.png");
                ChartUtilities.writeChartAsPNG(null, chart, 1000, 750,info);
                
                //ChartUtilities.saveChartAsPNG(file, chart, 1000, 750, info);
                        }
                         catch (Exception e) 
            {
                System.out.println(e);
            }
                        finally{
                            ServletContext context= getServletContext();
                            RequestDispatcher rd= context.getRequestDispatcher("/chartShow.jsp");
                            rd.forward(request, response);
            //  response.sendRedirect("chartShow.jsp");  
             }
                        
                    }
                    else if("ave. Rating Analysis".equals(request.getParameter("type")))
                    {
                        try{
            DefaultCategoryDataset  categoryDataset1 = new DefaultCategoryDataset();

                     String rating[]=new String[5],phone[]=new String[5];
                     String sql1="select phone_id,avg(rating) from rating group by phone_id order by avg(rating) desc";
                    rs=st.executeQuery(sql1);
                    i=0;
                    while(rs.next() && i<5){
                    rating[i]=rs.getString(2);
                    phone[i]=rs.getString(1);
                  //  out.println(Integer.parseInt(name[i]));
                   categoryDataset1.setValue(Float.parseFloat(rating[i]), "phone rating", phone[i]);
                    i++;
                    }
                    rs.close();
          //in('1960-61','1961-62','1962-63' ,'1963-64','1964-65','1965-66')
         JFreeChart chart1 = ChartFactory.createBarChart3D
                     (" ave. phones rating", // Title
                      "name",              // X-Axis label
                      "ave. rating",// Y-Axis label
                      categoryDataset1,         // Dataset
                      PlotOrientation.VERTICAL,
                      true,                     // Show legend
                      true,
                      false
                     );
                final ChartRenderingInfo info1 = new ChartRenderingInfo(new StandardEntityCollection());
                final File file1 = new File("C:/Users/Archit/Documents/NetBeansProjects/minor/minor/web/charts/barchart1.png");
                
                ChartUtilities.saveChartAsPNG(file1, chart1, 1000, 750, info1);
            }
            catch (Exception e) 
            {
                System.out.println(e);
            }
                 finally{
                             ServletContext context= getServletContext();
                            RequestDispatcher rd= context.getRequestDispatcher("/chartShow.jsp");
                            rd.forward(request, response);
                            // response.sendRedirect("chartShow.jsp");  
             }
                        
                    }
                    else if("Comment Analysis".equals(request.getParameter("type")))
                    {
                             
             try{
                 DefaultCategoryDataset  categoryDataset3 = new DefaultCategoryDataset();
           
                    String phone[]=new String[5],comments[]=new String[5];
                    String sql2="select count(*),phone_id from comment group by phone_id order by count(*) desc";
                    rs=st.executeQuery(sql2);
                    i=0;
                    while(rs.next() && i<5){
                    phone[i]=rs.getString(2);
                    comments[i]=rs.getString(1);
                   categoryDataset3.setValue(Integer.parseInt(comments[i]), "Comments", phone[i]);
                    i++;
                    }
                    rs.close();
                     JFreeChart chart3 = ChartFactory.createBarChart3D
                     ("Comment Analysis", // Title
                      "Phone",              // X-Axis label
                      "No. Of comments",// Y-Axis label
                      categoryDataset3,         // Dataset
                      PlotOrientation.VERTICAL,
                      true,                     // Show legend
                      true,
                      false
                     );
        
                final ChartRenderingInfo info3 = new ChartRenderingInfo(new StandardEntityCollection());
                final File file3 = new File("C:/Users/Archit/Documents/NetBeansProjects/minor/minor/web/charts/barchart2.png");
                
                ChartUtilities.saveChartAsPNG(file3, chart3, 1000, 750, info3);
          }
              catch (Exception e) 
            {
                System.out.println(e);
            }
             finally{
              ServletContext context= getServletContext();
                            RequestDispatcher rd= context.getRequestDispatcher("/chartShow.jsp");
                            rd.forward(request, response);
                 
                 // response.sendRedirect("chartShow.jsp");  
             }
                   
                    }
                    
                    
                    //else{ //if(request.getParameter("type").isEmpty()) {
                    try{   
                    DefaultCategoryDataset  categoryDataset2 = new DefaultCategoryDataset();
                    String rating[]=new String[5],date[]=new String[5];
                    String sql4="select avg(rating),date from rating where phone_id='nokia-106_3818' group by date order by avg(rating) desc";
                    ResultSet rs1=st.executeQuery(sql4);
                    i=0;
                    while(rs1.next() && i<5){
                    rating[i]=rs1.getString(1);
                    date[i]=rs1.getString(2);
                   categoryDataset2.setValue(Float.parseFloat(rating[i]), "Ave. Ratings", date[i]);
                    i++;
                    }
                    rs1.close();
      
            JFreeChart chart2 = ChartFactory.createBarChart3D
                     ("Rating Analysis", // Title
                      "date",              // X-Axis label
                      "Ave. Rating",// Y-Axis label
                      categoryDataset2,         // Dataset
                      PlotOrientation.VERTICAL,
                      true,                     // Show legend
                      true,
                      false
                     );
        
                final ChartRenderingInfo info2 = new ChartRenderingInfo(new StandardEntityCollection());
                final File file2 = new File("C:/Users/Archit/Documents/NetBeansProjects/minor/minor/web/charts/barchart8.png");
                
              //  ChartUtilities.saveChartAsPNG(file2, chart2, 1000, 750, info2);
                 }
            catch (Exception e) 
            {
                System.out.println(e);
            }
                    finally{
                   // response.sendRedirect("specs.jsp");
                    }
                   //}
                 
             
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
