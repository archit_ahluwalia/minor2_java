package webservice;
import java.util.List;
import javax.xml.ws.WebServiceRef;
import webservices.Ratings;
import webservices.RatingsService;

public class rating_1 {
  @WebServiceRef(wsdlLocation="http://localhost:8080/srishti/ratingsService?WSDL")
    static RatingsService service;
    Ratings port;
  public rating_1(){
        service=new RatingsService();
        port=service.getRatingsPort();
  }
   /* public static void main(String[] args){
        rating_1 r=new rating_1();
        System.out.println(r.getClientView("apple-ipad-2_1469")); 
        System.out.println(r.getClientRating("apple-ipad-2_1469","battery"));
        r.update("apple-ipad-2_1469","battery",4);
    }*/
    public int getClientView(String id){
    
        int view= port.getView(id);
        return view;
    }
   public List<Integer> getClientRating(String id,String table){
        List <Integer> rat;
        rat= null;
        rat=port.getRating(id, table);
        return rat;
   }
   public void update(String id,String table,int userRating){
        int r=port.updateRating(id, table, userRating);
}
  
}

  